package retrofit.utilz;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.google.android.material.snackbar.Snackbar;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AppPermissions {
    private Activity activity;
    private Fragment fragment;
    private PermissionCallback permissionCallback;
    private boolean isFragment;
    private static final int MY_PERMISSIONS_REQUEST = 0x12;
    private static final int MY_PERMISSIONS_REQUEST2 = 0x13;
    private String[] requestedPermissions;
    private String TAG = getClass().getSimpleName();
    private SnackBarRetry snackBarRetry;
    private static AppPermissions appPermissions;

    public static boolean needExplicitPermissionsCheck() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static AppPermissions getInstance(AppCompatActivity activity){
        return new AppPermissions(activity);
    }
    public static AppPermissions getInstance(Fragment fragment){
        return new AppPermissions(fragment);
    }


    private AppPermissions(AppCompatActivity activity) {
        this.activity = activity;
        isFragment = false;
        snackBarRetry = new SnackBarRetry();
        appPermissions=this;
    }

    private AppPermissions(Fragment fragment) {
        this.fragment = fragment;
        this.activity = fragment.getActivity();
        isFragment = true;
        snackBarRetry = new SnackBarRetry();
        appPermissions=this;
    }

    public AppPermissions checkSelfPermission(PermissionCallback invokedPermissionClass, String... permissionList) {

//        Log.e(TAG, "checkSelfPermission() called with: " + "invokedPermissionClass = [" + invokedPermissionClass + "], permissionList = [" + permissionList + "]");


        if (invokedPermissionClass == null)
            throw new IllegalArgumentException("AppPermissions must not be Null");
        if (permissionList == null || permissionList.length == 0)
            throw new IllegalArgumentException("Please add list of permissions");
        permissionCallback = invokedPermissionClass;
        if(needExplicitPermissionsCheck())
        {
            this.requestedPermissions = permissionList;
            checkPermissionStatus(permissionList);
        }
        else{
            Map<String, Boolean> permissions = new HashMap<>();
            for (String permission : permissionList) {
                permissions.put(permission, true);
            }
            permissionCallback.onRequestPermissionsSuccessResult(permissions);
        }
        return this;
    }

    private void checkPermissionStatus(String... permissionList) {
        Map<String, Boolean> permissions = new HashMap<>();
        for (String permission : permissionList) {
            permissions.put(permission, ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED);
        }
        shouldShowRequestPermissionRationale(permissions);
    }

    private void shouldShowRequestPermissionRationale(Map<String, Boolean> permissions) {

        List<String> requestPermission = new ArrayList<>();
        List<String> rational = new ArrayList<>();

        for (String permission : permissions.keySet()) {
            if (permissions.get(permission)) {
                requestPermission.add(permission);
            }
        }

        if (requestPermission.size() > 0) {
            for (String rationalPermission : requestPermission) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        rationalPermission)) {
                    rational.add(rationalPermission);
                }
            }

            if (rational.size() > 0) {
                permissionCallback.onRequestPermissionsFailureResult(rational, snackBarRetry);
            } else requestPermissions(requestPermission, false);
        } else {
            permissionCallback.onRequestPermissionsSuccessResult(permissions);
        }


    }


    private void requestPermissions(List<String> requestPermission, boolean isRetry) {

      //  Log.d(TAG, "requestPermissions() called with: " + "requestPermission = [" + requestPermission + "], isRetry = [" + isRetry + "]");

        List<String> blocked = new ArrayList<>();

        if (isRetry) {
            for (String blockedPermission : requestPermission) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        blockedPermission)) {
                    blocked.add(blockedPermission);
                }
            }

            if (blocked.size() > 0) {


                Snackbar snackbar = Snackbar
                        .make(activity.findViewById(android.R.id.content), "Please grant permissions from application settings", Snackbar.LENGTH_LONG)
                        .setAction("ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                                intent.setData(uri);
                                activity.startActivity(intent);
                            }
                        });
                snackbar.show();

                return;
            }


        }


        if (isFragment) {
            fragment.requestPermissions(requestPermission.toArray(new String[requestPermission.size()]),
                    MY_PERMISSIONS_REQUEST2);
        } else {
            ActivityCompat.requestPermissions(activity,
                    requestPermission.toArray(new String[requestPermission.size()]),
                    MY_PERMISSIONS_REQUEST);
        }


    }

    public static void onRequestPermissionsResult(int requestCode,
                                                  String permissions[], int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST:
                if(appPermissions!=null)
                    appPermissions.onRequestResult();

                break;
            case MY_PERMISSIONS_REQUEST2:
                if(appPermissions!=null)
                    appPermissions.onRequestResult();

                break;

        }


    }
    private void onRequestResult(){
        if(requestedPermissions!=null)
            checkPermissionDeniedStatus(requestedPermissions);
    }


    private void checkPermissionDeniedStatus(String... permissionList) {


        List<String> requestPermission = new ArrayList<>();
        List<String> rational = new ArrayList<>();
        List<String> denied = new ArrayList<>();


        Map<String, Boolean> permissions = new HashMap<>();
        for (String permission : permissionList) {

            permissions.put(permission, ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED);
            if (permissions.get(permission))
                requestPermission.add(permission);
        }


        if (requestPermission.size() > 0) {
            for (String rationalPermission : requestPermission) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        rationalPermission)) {
                    rational.add(rationalPermission);
                }

                if (!ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        rationalPermission)) {
                    denied.add(rationalPermission);
                }
            }

            if (rational.size() > 0) {
                permissionCallback.onRequestPermissionsFailureResult(rational, snackBarRetry);
            } else if (denied.size() > 0) {
                permissionCallback.onRequestPermissionsFailureResult(denied, snackBarRetry);
            } else requestPermissions(requestPermission, false);
        } else
            permissionCallback.onRequestPermissionsSuccessResult(permissions);
    }

    public class SnackBarRetry {
        public void retry(final List<String> requestPermission, String message) {
            Snackbar snackbar = Snackbar
                    .make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
                    .setAction("retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            requestPermissions(requestPermission, true);
                        }
                    });

            View snackbarView = snackbar.getView();
            TextView textView = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setMaxLines(5);
            snackbar.show();
        }
    }

    public interface PermissionCallback {
        void onRequestPermissionsSuccessResult(@NonNull Map<String, Boolean> permissions);

        void onRequestPermissionsFailureResult(@NonNull List<String> permissions, SnackBarRetry snackBar);

    }


}