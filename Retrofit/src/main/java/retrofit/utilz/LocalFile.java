package retrofit.utilz;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import androidx.annotation.RequiresApi;

import java.io.File;
import java.io.IOException;

public class LocalFile {

    private Context context;
    String mCurrentPhotoPath;

    public LocalFile(Context context) {
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public File createImageFile() throws IOException {
        // Create an image file name
      /*  String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";*/
        String timestamp = System.currentTimeMillis() + "";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(timestamp, ".jpg", storageDir);
        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }
}