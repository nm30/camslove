package retrofit.utilz

import retrofit.BuildConfig


/**
 * Created by Amandeep Singh on 12/28/2016 10:56.
 */

object Log {

    /**
     * Priority constant for the println method; use Log.v.
     */
    val VERBOSE = 2

    /**
     * Priority constant for the println method; use Log.d.
     */
    val DEBUG = 3

    /**
     * Priority constant for the println method; use Log.i.
     */
    val INFO = 4

    /**
     * Priority constant for the println method; use Log.w.
     */
    val WARN = 5

    /**
     * Priority constant for the println method; use Log.e.
     */
    val ERROR = 6

    /**
     * Priority constant for the println method.
     */
    val ASSERT = 7

    private val isShow = BuildConfig.DEBUG

    /**
     * Send a [.VERBOSE] log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    fun v(tag: String, msg: String): Int {
        return if (!isShow) -1 else android.util.Log.v(tag, msg)
    }

    /**
     * Send a [.VERBOSE] log message and log the exception.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr  An exception to log
     */
    fun v(tag: String, msg: String, tr: Throwable): Int {
        return if (!isShow) -1 else android.util.Log.v(tag, msg, tr)
    }

    /**
     * Send a [.DEBUG] log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    fun d(tag: String, msg: String): Int {

        /*       if (tag.contains("GoogleRecognitionListener")||tag.contains("SpeechRecognizerManager"))
            return -1;*/
        return if (!isShow) -1 else android.util.Log.d(tag, "$msg")
    }

    /**
     * Send a [.DEBUG] log message and log the exception.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr  An exception to log
     */
    fun d(tag: String, msg: String, tr: Throwable): Int {
        return if (!isShow) -1 else android.util.Log.d(tag, msg, tr)
    }

    /**
     * Send an [.INFO] log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    fun i(tag: String, msg: String): Int {
        return if (!isShow) -1 else android.util.Log.i(tag, msg)
    }

    /**
     * Send a [.INFO] log message and log the exception.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr  An exception to log
     */
    fun i(tag: String, msg: String, tr: Throwable): Int {
        return if (!isShow) -1 else android.util.Log.i(tag, msg, tr)
    }

    /**
     * Send a [.WARN] log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    fun w(tag: String, msg: String): Int {
        return if (!isShow) -1 else android.util.Log.w(tag, msg)
    }

    /**
     * Send a [.WARN] log message and log the exception.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr  An exception to log
     */
    fun w(tag: String, msg: String, tr: Throwable): Int {
        return if (!isShow) -1 else android.util.Log.w(tag, msg, tr)
    }   /**
     * Send a [.WARN] log message and log the exception.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr  An exception to log
     */
    fun w(tag: String, msg: String, tr: Exception): Int {
        return if (!isShow) -1 else android.util.Log.w(tag, msg, tr)
    }


    /*
     * Send a {@link #WARN} log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param tr An exception to log
     */
    fun w(tag: String, tr: Throwable): Int {
        return if (!isShow) -1 else android.util.Log.w(tag, "", tr)
    }

    /**
     * Send an [.ERROR] log message.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    fun e(tag: String, msg: String): Int {
        /*        if (tag.contains("GoogleRecognitionListener"))
            return -1;*/
        return if (!isShow) -1 else android.util.Log.e(tag, msg)
    }

    /**
     * Send a [.ERROR] log message and log the exception.
     *
     * @param tag Used to identify the source of a log message.  It usually identifies
     * the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr  An exception to log
     */
    fun e(tag: String, msg: String, tr: Throwable): Int {
        return if (!isShow) -1 else android.util.Log.e(tag, msg, tr)
    }

    fun d(tag: String, text: String, vararg args: Any): Int {
        return if (!isShow) -1 else d(tag, String.format(text, *args))
    }

    fun d(tag: String, t: Throwable, text: String, vararg args: Any): Int {
        return if (!isShow) -1 else d(tag, String.format(text, *args), t)
    }

    fun e(tag: String, t: Throwable, text: String, vararg args: Any): Int {
        return if (!isShow) -1 else e(tag, String.format(text, *args), t)
    }

    fun e(tag: String, text: String, vararg args: Any): Int {
        return if (!isShow) -1 else e(tag, String.format(text, *args))
    }

    fun v(tag: String, text: String, vararg args: Any): Int {
        return if (!isShow) -1 else v(tag, String.format(text, *args))

    }
}
