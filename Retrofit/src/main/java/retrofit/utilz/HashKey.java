package retrofit.utilz;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Amandeep Singh Bagli on 6/7/2017 @ 23:08.
 */

public class HashKey {
    /**
     * 2jmj7l5rSw0yVb/vlWAYkK/YBwk=
     * 07-20 00:09:11.181 22563-22563/? E/hash key: Jp5eiSPNZUBZdcm7zSPEpmHqjdQ=
     *
     * @param context
     */
    public static String findKey(Context context) {
        PackageInfo info;
        String key = "";
        try {
            info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                key = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("KeyHash:", key);

                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
                key =  something;
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
        return key;
    }
}

