package retrofit.utilz;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;
import androidx.annotation.DrawableRes;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;


/**
 * Created by Amandeep Singh Bagli on 2/8/2018 @ 10:46:39.
 */

public class ImageUtility {
    private Context context;
    private String TAG = "ImageUtility";


    private ImageUtility(Context context) {
        this.context = context;


    }

    public static ImageUtility with(Context context) {

        return new ImageUtility(context);

    }

    public Load load(String url) {
    //    Log.d(TAG, "load() called with: url = [" + url + "]");
        return new Load(url);
    }

    public Load load(@DrawableRes int resourceDrawable) {
      //  Log.d(TAG, "load() called with: url = [" + resourceDrawable + "]");
        return new Load(resourceDrawable);
    }

    public Load load(File file) {
        return new Load(file);
    }
    public Load load(Uri uri) {
        return new Load(uri);
    }

    public class Load {
        private String url;
        private int placeHolderResId = -1;
        private File file;
        @DrawableRes
        int resourceDrawable = -1;
        private Uri uri;

        public Load(@DrawableRes int resourceDrawable) {
            this.resourceDrawable = resourceDrawable;
        }

        public Load(String url) {

            this.url = url;
        }

        public Load(File file) {
            this.file = file;
        }

        public Load(Uri uri) {
            this.uri=uri;
        }

        public void into(ImageView imageView) {


           if (placeHolderResId == -1) {

                if (file != null)
                    Glide.with(context).load(file).into(imageView);
                else if (url != null)
                    Glide.with(context).load(url).into(imageView);
                else if (resourceDrawable != -1) {
                    Glide.with(context).load(resourceDrawable).into(imageView);
                } else if (uri != null) {
                    Glide.with(context).load(uri).into(imageView);
                }

            } else {
                if (file != null)
                    Glide.with(context)
                            .load(file)
                            .apply(
                                    new RequestOptions()
                                            .error(placeHolderResId)
                                            .placeholder(placeHolderResId)
                                            .fitCenter())
                            .into(imageView);
                else if (url != null)
                    Glide.with(context)
                            .load(url)
                            .apply(
                                    new RequestOptions()
                                            .error(placeHolderResId)
                                            .placeholder(placeHolderResId)
                                            .fitCenter())
                            .into(imageView);
                else if (resourceDrawable != -1) {
                    Glide.with(context)
                            .load(resourceDrawable)
                            .apply(
                                    new RequestOptions()
                                            .error(placeHolderResId)
                                            .placeholder(placeHolderResId)
                                            .fitCenter())
                            .into(imageView);
                }      else if (uri != null) {
                    Glide.with(context)
                            .load(uri)
                            .apply(
                                    new RequestOptions()
                                            .error(placeHolderResId)
                                            .placeholder(placeHolderResId)
                                            .fitCenter())
                            .into(imageView);
                }
            }
        }

        public Load placeHolder(int placeHolderResId) {
            this.placeHolderResId = placeHolderResId;

            return this;
        }
    }


}
