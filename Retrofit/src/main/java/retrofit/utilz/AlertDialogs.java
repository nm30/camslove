package retrofit.utilz;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class AlertDialogs {

    private String TAG = getClass().getSimpleName();
    private AppCompatActivity activity;

    public AlertDialogs(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void show(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
// Add the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // UserData clicked OK button
            }
        });
        /*builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // UserData cancelled the dialog
            }
        });*/
// Set other dialog properties

        builder.setMessage(message)
                .setTitle("Alert");
// Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        if (!((Activity) activity).isFinishing()) {
            dialog.show();
        }


    }

    public void showOk(final AppCompatActivity activity, String message, DialogInterface.OnClickListener dialogClickListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(
                activity).create();

        // Setting Dialog Title
        //  alertDialog.setTitle("Alert Dialog");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        // alertDialog.setIcon(R.drawable.tick);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", dialogClickListener);

        // Showing Alert Message
        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.show();
    }

    public void show(final AppCompatActivity activity, String message, String text, String text1, DialogInterface.OnClickListener dialogClickListener) {

        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

        alertDialog.setMessage(message);

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, text, dialogClickListener);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, text1, dialogClickListener);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);


    }

    public void showUpdate(final AppCompatActivity activity, String message, String text, DialogInterface.OnClickListener dialogClickListener) {

        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

        alertDialog.setMessage(message);

        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, text, dialogClickListener);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);


    }

}
