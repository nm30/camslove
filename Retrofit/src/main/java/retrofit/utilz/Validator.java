package retrofit.utilz;

import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Amandeep Singh Bagli on 06/09/16 at 6:29 PM.
 */

public class Validator {

    public static boolean textValidator(EditText editText) {
        if (editText.getText().toString().trim().isEmpty())
            return true;
        return false;
    }

    public static boolean dimValidator(EditText editText) {
        if (editText.getText().toString().isEmpty())
            return true;

        try {
            double value = Double.parseDouble(editText.getText().toString());
            if (value <= 0)
                return true;
            return false;

        } catch (Exception e) {
            //Log.e("dimValidator", "empty lenghtEditText", e);

        }
        return true;

    }

    public static boolean nameValidator(EditText editText) {
        if (editText.getText().toString().trim().isEmpty() || editText.getText().toString().length() < 2)
            return true;
        return false;
    }

    public static boolean specialCharacterValidator(EditText editText) {
        Pattern regex = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!-]");
        if (editText.getText().toString().trim().isEmpty() || editText.getText().toString().contains(" ") || editText.getText().toString().length() < 2 || regex.matcher(editText.getText().toString()).find()) {
            //handle your action here toast message/ snackbar or something else
            return true;
        }
        return false;
    }

    public static boolean isValidPin(final String pin) {

        if (BaseUtil.isNullOrEmpty(pin) || pin.length() < 8 || pin.contains(" "))
            return false;

        final String PIN_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        Pattern pattern = Pattern.compile(PIN_PATTERN);
        Matcher matcher = pattern.matcher(pin);

        return matcher.matches();

    }

    public static boolean ValidSuperUser(final EditText editText) {


        if (editText.getText().toString().trim().isEmpty() || editText.getText().toString().length() < 6 || editText.getText().toString().contains(" ")) {

            return true;
        }

        return false;

    }

    public static boolean userNameSpaceValidator(EditText editText) {
        if (editText.getText().toString().contains(" ") || editText.getText().toString().length() < 2)
            return true;
        return false;
    }

    public static boolean validateEmployeeID(EditText editText) {


        if (editText.getText().toString().trim().isEmpty() || editText.getText().toString().length() < 4) {

            return true;
        }

        return false;
    }

    public static boolean zipCodeValidator(EditText editText) {


        if (editText.getText().toString().trim().isEmpty() || editText.getText().toString().length() < 4) {

            return true;
        }

        return false;
    }

    public static boolean equalValidator(EditText editText1, EditText editText2) {


        if (editText1.getText().toString().trim().isEmpty() || editText2.getText().toString().trim().isEmpty()) {
            return false;
        }
        if (editText1.getText().toString().equals(editText2.getText().toString()))
            return true;

        return false;
    }

    public static boolean otpValidator(EditText editText) {
        if (editText.getText().toString().trim().isEmpty() || editText.getText().toString().length() < 5)
            return true;
        return false;
    }

    public static boolean passwordValidator(EditText editText) {
        if (editText.getText().toString().isEmpty())
            return true;

        if (editText.getText().toString().length() < 5)
            return true;

        return false;

    }

    public static boolean lengthValidator(EditText editText, int minLength) {
        if (editText.getText().toString().isEmpty())
            return true;

        if (editText.getText().toString().length() < minLength)
            return true;

        return false;

    }

    public static boolean emailValidator(EditText editText) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-+]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9\\-]+)*(\\.[A-Za-z]{2,})$";
//        if (editText.getText().toString().isEmpty())
//            return true;
        Pattern pattern;
        pattern = Pattern.compile(EMAIL_PATTERN);

        Matcher matcher = pattern.matcher(editText.getText().toString().trim());

        return !matcher.matches();
        //  return !TextUtils.isEmpty(editText.getText().toString()) && android.util.Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString()).matches();     //   return false;
    }

    public static boolean numberValidator(EditText editText) {

        if (editText.getText().toString().isEmpty())
            return true;
        //this is validation for SA
//        String pat="^(\\+?27|0)[6-9][1-7][0-9]{7}$";
//        Pattern pattern=Pattern.compile(pat);
//       return !pattern.matcher(editText.getText()).matches();
        if (editText.getText().toString().trim().replaceAll("[^0-9]", "").length() < 10)
            return true;

   /*     if (editText.getText().toString().trim().replaceAll("[^0-9]", "").toString().charAt(0) == '0')
            return true;*/
        return false;
    }

    public static boolean varificationCodeValidator(EditText editText) {

        if (editText.getText().toString().isEmpty())
            return true;
        String pat = "^[0-9]{6}$";
        Pattern pattern = Pattern.compile(pat);
        return !pattern.matcher(editText.getText()).matches();
//        if (editText.getText().toString().trim().replaceAll("[^0-9]", "").length() != 10)
//            return true;
//        if (editText.getText().toString().trim().replaceAll("[^0-9]", "").toString().charAt(0) == '0')
//            return true;
//        return false;
    }

}
