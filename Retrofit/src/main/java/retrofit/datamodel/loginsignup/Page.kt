package retrofit.datamodel.loginsignup

import com.google.gson.annotations.SerializedName
import retrofit.base.BaseData
import retrofit.base.ListModel

/**
 * Created by Manish Bhargav
 */

data class Page(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("page_label")
    val pageLabel: String? = null
) : ListModel<BaseData>()