package retrofit.datamodel.loginsignup

import com.google.gson.annotations.SerializedName
import retrofit.base.BaseData
import retrofit.base.DataModel
import retrofit.base.ListModel

/**
 * Created by Manish Bhargav
 */

data class Language(

    @field:SerializedName("lang_code")
    val langCode: String? = null,

    @field:SerializedName("language")
    val language: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
) : ListModel<BaseData>()