package retrofit.datamodel.loginsignup

import com.google.gson.annotations.SerializedName
import retrofit.base.BaseData
import retrofit.base.DataModel

/**
 * Created by Manish Bhargav
 */

 class LoginToken(

   @field:SerializedName("password")
   val password: String? = null,

   @field:SerializedName("role")
   val role: String? = null,

   @field:SerializedName("biz_role")
   var bizRole: String? = null,

   @field:SerializedName("opponentID")
   var opponentId: String? = null,

   @field:SerializedName("is_active")
   val isActive: Int? = null,

   @field:SerializedName("id")
   val id: Int? = null,

   @field:SerializedName("lease")
   val lease: String? = null,

   @field:SerializedName("secret")
   val secret: String? = null,

   @field:SerializedName("email")
   val email: String? = null,

   @field:SerializedName("username")
   var username: String? = null,

   @field:SerializedName("organization")
   val organization: Organization? = null,

   @field:SerializedName("token")
   val token: String? = null
) :  DataModel<BaseData>()
