package retrofit.datamodel.loginsignup

import com.google.gson.annotations.SerializedName
import retrofit.base.BaseData
import retrofit.base.ListModel

/**
 * Created by Manish Bhargav
 */

data class ScreenString(

	@field:SerializedName("page_id")
	val pageId: Int? = null,

	@field:SerializedName("language")
	val language: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("value")
	val value: String? = null,

	@field:SerializedName("key")
	val key: String? = null
) : ListModel<BaseData>()