package retrofit.datamodel.loginsignup

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import retrofit.base.BaseData
import retrofit.base.ListModel

/**
 * Created by Manish Bhargav
 */

@Parcelize
data class StaticPage(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("body")
	val body: String? = null
) : Parcelable,ListModel<BaseData>()
