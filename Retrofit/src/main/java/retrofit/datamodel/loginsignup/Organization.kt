package retrofit.datamodel.loginsignup

import com.google.gson.annotations.SerializedName

/**
 * Created by Manish Bhargav
 */

data class Organization(

	@field:SerializedName("license")
	val license: String? = null,

	@field:SerializedName("theme_bg")
	val themeBg: String? = null,

	@field:SerializedName("theme_col")
	val themeCol: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("secret")
	val secret: String? = null,

	@field:SerializedName("validity")
	val validity: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)