package retrofit.datamodel.dashboard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import retrofit.base.BaseData
import retrofit.base.ListModel

/**
 * Created by Manish Bhargav
 */

@Parcelize
data class FilterRes(

    @field:SerializedName("role")
    val role: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: String? = null
) : Parcelable, ListModel<BaseData>()