package retrofit.datamodel.dashboard

import com.google.gson.annotations.SerializedName
import retrofit.base.BaseData
import retrofit.base.DataModel

/**
 * Created by Manish Bhargav
 */

data class FileUploadRes(

    @field:SerializedName("file")
    val file: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
) : DataModel<BaseData>()