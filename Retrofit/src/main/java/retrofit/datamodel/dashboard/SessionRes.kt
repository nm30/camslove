package retrofit.datamodel.dashboard

import com.google.gson.annotations.SerializedName
import retrofit.base.BaseData
import retrofit.base.DataModel

/**
 * Created by Manish Bhargav
 */

data class SessionRes(

    @field:SerializedName("duration")
    val duration: String? = null,

    @field:SerializedName("opponentID")
    val opponentID: String? = null,

    @field:SerializedName("user_id")
    val userId: Int? = null,

    @field:SerializedName("session_date")
    val sessionDate: String? = null,

    @field:SerializedName("solicitor_id")
    val solicitorId: Int? = null,

    @field:SerializedName("case_id")
    val caseId: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("token_session")
    val tokenSession: String? = null
) : DataModel<BaseData>()