package retrofit.datamodel.dashboard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import retrofit.base.BaseData
import retrofit.base.ListModel

@Parcelize
data class UkCounties(

    @field:SerializedName("code")
    val code: String? = null,

    @field:SerializedName("county")
    val county: String? = null,

    @field:SerializedName("value")
    val value: String? = null,

    @field:SerializedName("int")
    val id: Int? = null
) : Parcelable, ListModel<BaseData>()
