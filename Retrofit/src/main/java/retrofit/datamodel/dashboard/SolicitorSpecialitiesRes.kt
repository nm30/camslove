package retrofit.datamodel.dashboard

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import retrofit.base.BaseData
import retrofit.base.ListModel

/**
 * Created by Manish Bhargav
 */

@Parcelize
data class SolicitorSpecialitiesRes(

    @field:SerializedName("speciality")
    val speciality: String? = null,

    @field:SerializedName("language")
    val language: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
) : Parcelable, ListModel<BaseData>()