package retrofit.datamodel.dashboard

import com.google.gson.annotations.SerializedName
import retrofit.base.BaseData
import retrofit.base.ListModel

/**
 * Created by Manish Bhargav
 */

data class CasesList(

    @field:SerializedName("rejected_date")
    val rejectedDate: String? = null,

    @field:SerializedName("user_id")
    val userId: Int? = null,

    @field:SerializedName("document")
    val document: String? = null,

    @field:SerializedName("solicitor_id")
    val solicitorId: Int? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("accepted_date")
    val acceptedDate: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("date_created")
    val dateCreated: String? = null,

    @field:SerializedName("closed_date")
    val closedDate: String? = null,

    @field:SerializedName("paid_date")
    val paidDate: String? = null
) : ListModel<BaseData>()