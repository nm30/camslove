package retrofit.datamodel.dashboard

import com.google.gson.annotations.SerializedName
import retrofit.base.BaseData
import retrofit.base.ListModel

/**
 * Created by Manish Bhargav
 */

data class UserRoleList(

    @field:SerializedName("solicitor_years")
    val solicitorYears: Int? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("role")
    val role: String? = null,

    @field:SerializedName("solicitor_image")
    val solicitorImage: String? = null,

    @field:SerializedName("timeregistered")
    val timeRegistered: String? = null,

    @field:SerializedName("last_name")
    val lastName: String? = null,

    @field:SerializedName("solicitor_specialities")
    val solicitorSpecialities: String? = null,

    @field:SerializedName("password")
    val password: String? = null,

    @field:SerializedName("solicitor_price")
    val solicitorPrice: String? = null,

    @field:SerializedName("solicitor_status")
    val solicitorStatus: String? = null,

    @field:SerializedName("solicitor_descprition")
    val solicitorDescription: String? = null,

    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("first_name")
    val firstName: String? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("solicitor_company")
    val solicitorCompany: String? = null,

    @field:SerializedName("username")
    val username: String? = null,

    @field:SerializedName("opponentID")
    val opponentId: String? = null,

    @field:SerializedName("company")
    val company: String? = null
) : ListModel<BaseData>()