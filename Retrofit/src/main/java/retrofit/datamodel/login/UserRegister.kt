package retrofit.datamodel.login

import com.google.gson.annotations.SerializedName

data class UserRegister(

	@field:SerializedName("country_code")
	val countryCode: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("user_type")
	val userType: Int? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("device_token")
	val deviceToken: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("device_type")
	val deviceType: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("signup_type")
	val signupType: Int? = null
)
