package retrofit.datamodel.login

import com.google.gson.annotations.SerializedName
import retrofit.base.BaseData
import retrofit.base.BaseModel
import retrofit.base.DataModel

data class UserLoginResponse(

    @field:SerializedName("chat_subscribe")
    val chatSubscribe: Int? = null,

    @field:SerializedName("gender")
    val gender: Int? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("device_type")
    val deviceType: String? = null,

    @field:SerializedName("is_unreadnotif")
    val isUnreadnotif: Int? = null,

    @field:SerializedName("used_referral_code")
    val usedReferralCode: String? = null,

    @field:SerializedName("liked_byme")
    val likedByme: Int? = null,

    @field:SerializedName("user_type")
    val userType: Int? = null,

    @field:SerializedName("is_deleted")
    val isDeleted: Int? = null,

    @field:SerializedName("verificationreq_sent")
    val verificationreqSent: Int? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("deleted_reason")
    val deletedReason: String? = null,

    @field:SerializedName("usermetas")
    val usermetas: List<Any?>? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("first_name")
    val firstName: String? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("socket_id")
    val socketId: String? = null,

    @field:SerializedName("is_verify")
    val isVerify: Int? = null,

    @field:SerializedName("last_name")
    val lastName: String? = null,

    @field:SerializedName("fav_byme")
    val favByme: Int? = null,

    @field:SerializedName("user_hobby")
    val userHobby: List<Any?>? = null,

    @field:SerializedName("blocked_byme")
    val blockedByme: Int? = null,

    @field:SerializedName("signup_type")
    val signupType: Int? = null,

    @field:SerializedName("token")
    val token: String? = null,

    @field:SerializedName("country_code")
    val countryCode: String? = null,

    @field:SerializedName("phone")
    val phone: String? = null,

    @field:SerializedName("last_visited_at")
    val lastVisitedAt: String? = null,

    @field:SerializedName("device_token")
    val deviceToken: String? = null,

    @field:SerializedName("is_unread")
    val isUnread: Int? = null,

    @field:SerializedName("deactivate_reason")
    val deactivateReason: String? = null,

    @field:SerializedName("referral_code")
    val referralCode: String? = null,

    @field:SerializedName("online")
    val online: Int? = null,

    @field:SerializedName("status")
    val status: Int? = null
) : BaseData()
