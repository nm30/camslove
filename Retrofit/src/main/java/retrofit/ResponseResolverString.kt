package retrofit


import retrofit.utilz.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ProtocolException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by Amandeep Singh on 1/9/2017 11:14.
 */

abstract class ResponseResolverString : Callback<String> {
    private val TAG = "ResponseResolverString"

    override fun onResponse(call: Call<String>, response: Response<String>) {

        if (response.isSuccessful && response.body() != null) {
            val baseModel = response.body()

            if (baseModel != null) {
                onSuccessResponse(response.body()!!)
            } else {
                onFailureResponse(baseModel + "", response.code())
            }

            //success block
        } else if (response.errorBody() != null) {


            onFailureResponse(response.message(), response.code())

            //error block
        } else {
            //error block
            onFailureResponse(response.message(), response.code())
        }

    }

    override fun onFailure(call: Call<String>, throwable: Throwable) {
        Log.d(TAG, "onFailure() called with: call = [$call], throwable = [$throwable]")


        if (throwable is UnknownHostException || throwable is ProtocolException) {
            onFailureResponse("Please connect to internet!!!", 100)
        } else if (throwable is SocketTimeoutException) {
            onFailureResponse("Server not responding!!!", 100)
        } else
            onFailureResponse(
                if (throwable.message == null) "Something went wrong!!!" else throwable.message + "",
                100
            )
    }


    abstract fun onSuccessResponse(jsonString: String)

    abstract fun onFailureResponse(message: String, errorCode: Int)
}