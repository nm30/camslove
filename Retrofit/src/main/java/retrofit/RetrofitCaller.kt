package retrofit

import retrofit.base.APICallback
import retrofit.base.BaseData
import retrofit.communications.APICall
import retrofit.communications.APIHelper


/**
 * Created by Amandeep Singh Bagli on 10/24/2017 @ 3:59 PM.
 */
class RetrofitCaller {
    companion object {
        fun build(callback: APICallback<BaseData>): APIHelper {

            return APICall.build(callback)
        }
    }
}