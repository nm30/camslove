package retrofit


import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import retrofit.contants.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

/**
 * Created by Amandeep Singh on 12/28/2016 12:17.
 */

class RetrofitClient(private val context: Context) {

    /*
    public Retrofit retrofitBuilder() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().readTimeout(2, TimeUnit.MINUTES).retryOnConnectionFailure(true)
                .connectTimeout(2, TimeUnit.MINUTES).addInterceptor(interceptor).build();

        //  .baseUrl("https://api.github.com/")
        final Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(url)
                .addConverterFactory(ToStringConverterFactory.create()).build();
//"https://cherrypickprices.com/beta/cpp/services/"
        return retrofit;
    }
*/


    fun retrofitBuilder(): Retrofit {
        val interceptor = HttpLoggingInterceptor();
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true).addInterceptor(interceptor).build()

        return Retrofit.Builder().client(client).baseUrl(url)
            .addConverterFactory(ToStringConverterFactory.create()).build()
    }

    fun retrofitModelBuilder(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true).addInterceptor(interceptor).build()
        //  .baseUrl("https://api.github.com/")

        val client3 = OkHttpClient.Builder()
            .protocols(Arrays.asList(Protocol.HTTP_2, Protocol.HTTP_1_1))
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()


        return Retrofit.Builder()
            .client(client)
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }


    fun retrofitSSLModelBuilder(): Retrofit {
/*
        // loading CAs from an InputStream
        val cf = CertificateFactory.getInstance("X.509")
        val cert = context.resources.openRawResource(R.raw.timeinator)
        val ca: Certificate
        try {
            ca = cf.generateCertificate(cert)
        } finally {
            cert.close()
        }

        // creating a KeyStore containing our trusted CAs
        val keyStoreType = KeyStore.getDefaultType()
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null, null)
        keyStore.setCertificateEntry("ca", ca)

        // creating a TrustManager that trusts the CAs in our KeyStore
        val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
        val tmf = TrustManagerFactory.getInstance(tmfAlgorithm)
        tmf.init(keyStore)

        // creating an SSLSocketFactory that uses our TrustManager
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, tmf.getTrustManagers(), null)


*/

        val certificateFactory = CertificateFactory.getInstance("X.509")
        val certFile = 111;//R.raw.time
        val inputStream = context.resources.openRawResource(certFile) //(.crt)

        val certificate = certificateFactory.generateCertificate(inputStream)
        inputStream.close()


        // Create a KeyStore containing our trusted CAs
        val keyStoreType = KeyStore.getDefaultType()
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null, null)
        keyStore.setCertificateEntry("ca", certificate)


        // Create a TrustManager that trusts the CAs in our KeyStore.
        val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()

        val trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm)
        trustManagerFactory.init(keyStore)


        val trustManagers = trustManagerFactory.trustManagers
        val x509TrustManager = trustManagers[0] as X509TrustManager
        // Create an SSLSocketFactory that uses our TrustManager
        val sslContext = SSLContext.getInstance("TLS")
        // sslContext.init(null, arrayOf<TrustManager>(x509TrustManager), null)
        sslContext.init(null, trustManagers, null)
        // val sslContext = getSSLConfig()
        val clientBuilder = OkHttpClient.Builder()

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        var sslSocketFactory = sslContext.socketFactory

        clientBuilder.readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .sslSocketFactory(sslSocketFactory, x509TrustManager)
            .retryOnConnectionFailure(true)
            .addInterceptor(interceptor)

        //   val clientBuilder = trustCert()

        clientBuilder.hostnameVerifier(object : HostnameVerifier {
            override fun verify(hostname: String, session: SSLSession): Boolean {

                return true
            }
        })
        val client = clientBuilder.build()

        /*val certificateFactory = CertificateFactory.getInstance("X.509")

        val inputStream = context.resources.openRawResource(R.raw.time) //(.crt)
        val certificate = certificateFactory.generateCertificate(inputStream)
        inputStream.close()

        // Create a KeyStore containing our trusted CAs
        val keyStoreType = KeyStore.getDefaultType()
        val keyStore = KeyStore.getInstance(keyStoreType)
        keyStore.load(null, null)
        keyStore.setCertificateEntry("ca", certificate)

        // Create a TrustManager that trusts the CAs in our KeyStore.
        val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
        val trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm)
        trustManagerFactory.init(keyStore)

        val trustManagers = trustManagerFactory.trustManagers
        val x509TrustManager = trustManagers[0] as X509TrustManager


        // Create an SSLSocketFactory that uses our TrustManager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, arrayOf<TrustManager>(x509TrustManager), null)
        var sslSocketFactory = sslContext.socketFactory

        *//*    //create Okhttp client
            val client = OkHttpClient.Builder()
                    .sslSocketFactory(sslSocketFactory, x509TrustManager)
                    .build()*//*


        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder().readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS).sslSocketFactory(sslSocketFactory, x509TrustManager)
                .retryOnConnectionFailure(true).addInterceptor(interceptor).build()*/
        //  .baseUrl("https://api.github.com/")


//"https://cherrypickprices.com/beta/cpp/services/"
        return Retrofit.Builder()
            .client(client)
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }


    companion object {
        ///  private static final String url = Config.getBaseUrl();
        private val url = Config.baseUrl
    }
}
