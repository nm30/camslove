package retrofit


//import com.timinator.app.retrofit.base.APICallback
//import com.timinator.app.retrofit.base.APIViewModel
//import com.timinator.app.retrofit.base.BaseData
//import com.timinator.app.retrofit.base.BaseModel
//import com.timinator.app.utilz.BaseUtil
//import com.timinator.app.utilz.Log


import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import retrofit.base.APICallback
import retrofit.base.APIViewModel
import retrofit.base.BaseData
import retrofit.base.BaseModel
import retrofit.utilz.BaseUtil
import retrofit.utilz.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ProtocolException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*


/*object ViewModelUtil {
    fun <T : ViewModel> createFor(model: T): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                if (modelClass.isAssignableFrom(model.javaClass)) {
                    @Suppress("UNCHECKED_CAST")
                    return model as T
                }
                throw IllegalArgumentException("unexpected model class $modelClass")
            }
        }
    }
}*/
/**
 * Created by Amandeep Singh on 1/9/2017 11:14.
 */

abstract class ResponseResolver<Model : BaseModel<*>, in CallBackAPI : APICallback<BaseData>> :
    Callback<Model>,
    LifecycleObserver {
    private val TAG = javaClass.simpleName
    protected var activity: AppCompatActivity? = null
    private var fragment: Fragment? = null
    private lateinit var apiViewModel: APIViewModel
    private var list: List<BaseModel<*>>? = null

    private var responseQueue: Queue<BaseModel<*>>? = null
    private var isForeground = true;

    constructor(callback: CallBackAPI) {
        if (callback is AppCompatActivity) {
            activity = callback
            activity?.lifecycle?.addObserver(this)
        } else if (callback is Fragment) {
            fragment = callback
            fragment?.lifecycle?.addObserver(this)
            activity = fragment?.activity as AppCompatActivity
        } else {
            throw IllegalArgumentException("Call back must be implemented with activity or fragment class!!!")
        }
        //  apiViewModel=createFor(apiViewModel).create(apiViewModel)
        /*var observer: Observer<BaseModel<BaseData>> = object : Observer<BaseModel<BaseData>> {
               override fun onChanged(data: BaseModel<BaseData>?) {
                   //("not implemented") //To change body of created functions use File | Settings | File Templates.
                   Log.e(TAG," Observer $data")

                   onSuccessResponse(data as Model)
               }
           }
           if (activity != null) {
               apiViewModel = ViewModelProviders.of(activity as FragmentActivity).get(APIViewModel::class.java)
               apiViewModel.getBaseData().observe(activity as FragmentActivity, observer)
           } else {
               apiViewModel = ViewModelProviders.of(fragment as Fragment).get(APIViewModel::class.java)
               apiViewModel.getBaseData().observe(fragment as Fragment, observer)
           }*/

        /*  var nameObserver:  Observer<String> =  Observer<String>() {
             @Override
             public void onChanged(@Nullable   String newName) {
                 // Update the UI, in this case, a TextView.
                 mNameTextView.setText(newName);
             }
         };*/


        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        //  mModel.getCurrentName().observe(this, nameObserver)

    }


    /*   fun <T1 : APIViewModel<Model>> createFor(model: T1): ViewModelProvider.Factory {
           return object : ViewModelProvider.Factory {
               override fun  create(modelClass: Class<T1>): T1 {
                   if (modelClass.isAssignableFrom(model.javaClass)) {
                       @Suppress("UNCHECKED_CAST")
                       return model as T1
                   }
                   throw IllegalArgumentException("unexpected model class $modelClass")
               }
           }
       }*/

    protected fun isScreenVisible() = this.isForeground

    override fun onResponse(call: Call<Model>, response: Response<Model>) {

        if (response.isSuccessful && response.body() != null) {
            val baseModel = response.body()
            Log.e(TAG, "MODEL $baseModel")
            if (response.code() in 199..310)
            /* if (baseModel!!.getResponseCode() == 200) */ {
                val responseModel = response.body()!!
                responseModel.updateResponseCode(response.code())
                onSuccessCall(responseModel)
            } else {

                onFailureCall(
                    BaseModel<BaseData>(
                        baseModel!!.getMessage() + "",
                        baseModel!!.getResponseCode()
                    ) as Model
                )
            }
            //success block
        } else if (response.errorBody() != null) {
            val errorJson = response.errorBody()!!.string();
            Log.e(TAG, "ERROR BODY $errorJson")
            var error: BaseModel<*>? = null
//TO DO need to add Converter
            error = BaseUtil.objectFromString(errorJson, BaseModel::class.java)

            if (error != null)
                onFailureCall(
                    BaseModel<BaseData>(
                        error.getMessage() + "",
                        error.getResponseCode()
                    ) as Model
                )
            else
                onFailureCall(BaseModel<BaseData>("Server not responding!", 100) as Model)

            //error block
        } else {
            //error block
            onFailureCall(BaseModel<BaseData>(response.message(), response.code()) as Model)
        }
    }

    override fun onFailure(call: Call<Model>, throwable: Throwable) {
        Log.d(TAG, "onFailure() called with: call = [$call], throwable = [$throwable]")

        if (throwable is UnknownHostException || throwable is ProtocolException) {
            onFailureCall(BaseModel<BaseData>("Please connect to internet!!!", 100) as Model)
        } else if (throwable is SocketTimeoutException || throwable is java.net.ConnectException) {
            onFailureCall(BaseModel<BaseData>("Server not responding!!!", 103) as Model)
        } else
            onFailureCall(
                BaseModel<BaseData>(
                    if (throwable.message == null) "Something went wrong!!!" else throwable.message + "",
                    101
                ) as Model
            )
    }

    private fun checkCode(errorCode: Int): Boolean {

        if ("SplashScreenActivity".equals(activity?.javaClass?.simpleName)) {
            return true
        }
        if (errorCode == 403) {
//            Toast.makeText(activity, "Unauthorised", Toast.LENGTH_LONG).show()
//            //TO DO redirect to splash screen
//            BaseUtil.AppExit(activity, "Session Expired,Please login again!")
//            Log.e(TAG, "onFailureCall checkCode: " + errorCode)
            return true//false change to true
        }
        return true
    }

    private fun onSuccessCall(model: Model) {
        Log.e(TAG, "model:  $model")
        //   apiViewModel.getBaseData().value=model
        onSuccessResponse(model)
        if (isScreenVisible()) {
            //   apiViewModel.getBaseData().value=model
            //    onSuccessResponse(model)
        } else {

            Log.e(TAG, "onSuccessCall No Screen Activity: " + activity?.javaClass?.simpleName)

        }

    }

    private fun onFailureCall(model: Model) {

        val isVisible = isScreenVisible()
        val auth = checkCode(model.getResponseCode())


        Log.e(
            TAG,
            "FAIL MODEL ${model.getMessage()}   ${model.getResponseCode()}  VISIBLE-> $isVisible  AUTH-> $auth "
        )
        if (auth) {
            onFailureResponse(model.getMessage(), model.getResponseCode())
        } else {
            Log.e(TAG, "onFailureCall No Screen Activity: " + activity?.javaClass?.simpleName)

        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun connectListener() {
        Log.e(TAG, "Lifecycle.Event.ON_RESUME")
        isForegroundComponent(true)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun disconnectListener() {
        Log.e(TAG, "Lifecycle.Event.ON_PAUSE")
        isForegroundComponent(false)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun startListener() {
        Log.e(TAG, "Lifecycle.Event.ON_START")
        isForegroundComponent(true)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stopListener() {
        Log.e(TAG, "Lifecycle.Event.ON_STOP")
        isForegroundComponent(false)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreateListener() {
        Log.e(TAG, "Lifecycle.Event.ON_CREATE")
        isForegroundComponent(true)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroyListener() {
        Log.e(TAG, "Lifecycle.Event.ON_DESTROY")
        isForegroundComponent(false)
    }

    private fun isForegroundComponent(yes: Boolean) {
        this.isForeground = yes

        /*   if(isForeground&&!responseQueue?.isNullOrEmpty()!!){

           }*/
    }

    abstract fun onSuccessResponse(model: Model)

    abstract fun onFailureResponse(message: String, errorCode: Int)
}
