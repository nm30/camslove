package retrofit.model

import com.google.gson.annotations.SerializedName

data class NotificationAndSupport(

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("isActive")
	val isActive: Boolean? = null
)
