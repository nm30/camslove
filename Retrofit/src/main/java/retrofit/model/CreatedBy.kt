package retrofit.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Manish Bhargav
 */

data class CreatedBy(

    @field:SerializedName("kind")
    val kind: String? = null,

    @field:SerializedName("creatorId", alternate = ["user"])
    val creatorId: String? = null
)