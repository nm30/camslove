package retrofit.model

import androidx.annotation.DrawableRes
import com.google.gson.annotations.SerializedName

data class Language(

	@field:SerializedName("url")
	@DrawableRes
	val url: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("isSelected")
	val isSelected: Boolean? = null
)
