package retrofit.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Manish Bhargav
 */

data class GroupVideoDetails(

    @field:SerializedName("_id")
    val id: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("category")
    val category: String? = null,

    @field:SerializedName("language")
    val language: String? = null,

    @field:SerializedName("gender")
    val gender: String? = null,

    @field:SerializedName("privacy")
    val privacy: String? = null,

    @field:SerializedName("youtube")
    val youtube: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("cover")
    val cover: String? = null,

    @field:SerializedName("video")
    val video: String? = null,

    @field:SerializedName("createdAt")
    val createdAt: String? = null,

    @field:SerializedName("updatedAt")
    val updatedAt: String? = null,

    @field:SerializedName("__v")
    val V: Int? = null,

    @field:SerializedName("isFavourite")
    var isFavourite: Boolean? = false,

    @field:SerializedName("favouriteId")
    val favouriteId: String? = null,

    @field:SerializedName("createdBy")
    val createdBy: CreatedBy? = null

)