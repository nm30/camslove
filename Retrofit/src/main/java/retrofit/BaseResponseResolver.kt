package retrofit

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import retrofit.base.APICallback
import retrofit.base.BaseData
import retrofit.base.BaseModel
import retrofit.utilz.BaseUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ProtocolException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by Amandeep Singh on 22/06/2018 11:14.
 */

abstract class BaseResponseResolver<Model : BaseModel<*>, in CallBackAPI : APICallback<BaseData>> :
    Callback<Model> {
    private val TAG = javaClass.simpleName
    protected var activity: AppCompatActivity? = null
    private var fragment: androidx.fragment.app.Fragment? = null

    constructor(callback: CallBackAPI) {
        if (callback is AppCompatActivity) {
            activity = callback
        } else if (callback is androidx.fragment.app.Fragment) {
            fragment = callback
        } else {
            throw IllegalArgumentException("Call back must be implemented with activity or fragment class!!!")
        }

    }

    protected fun isScreenVisible(): Boolean {
        var status: Boolean
        if (fragment == null) {
            /* if (activity!!.isFinishing()) {
                 status = false
             } else {
                 status = true
             }*/
            status = activity!!.isFinishing

        } else {
            status = fragment!!.isAdded


        }
        /*     if (!status)
                           ProgressDialog.dismissLoadingDialog()*/
        return status
    }

    override fun onResponse(call: Call<Model>, response: Response<Model>) {

        if (response.isSuccessful && response.body() != null) {
            val baseModel = response.body()
            //  onSuccessResponse(response.body()!!)
            if (response.code() == 200) {
                onSuccessCall(response.body()!!)
            } else {
                onFailureResponse(baseModel!!.getMessage() + "", baseModel.getResponseCode())
            }


            //success block
        } else if (response.errorBody() != null) {


            onFailureResponse(response.message(), response.code())

            //error block
        } else {
            //error block
            onFailureResponse(response.message(), response.code())
        }

    }

    override fun onFailure(call: Call<Model>, throwable: Throwable) {
        Log.d(TAG, "onFailure() called with: call = [$call], throwable = [$throwable]")


        if (throwable is UnknownHostException || throwable is ProtocolException) {
            onFailureCall("Please connect to internet!!!", 100)
        } else if (throwable is SocketTimeoutException) {
            onFailureCall("Server not responding!!!", 100)
        } else
            onFailureCall(
                if (throwable.message == null) "Something went wrong!!!" else throwable.message + "",
                100
            )
    }

    private fun checkCode(errorCode: Int) {
        if (errorCode == 403) {
            BaseUtil.AppExit(activity, "Session expired login again!")
            Log.e(TAG, "onFailureCall checkCode: " + errorCode)
        }
    }

    private fun onSuccessCall(model: Model) {

        if (isScreenVisible()) {
            onSuccessResponse(model)
        } else {
            Log.e(TAG, "onSuccessCall No Screen Activity: " + activity?.javaClass?.simpleName)
            Log.e(TAG, "onSuccessCall No Screen Fragment: " + activity?.javaClass?.simpleName)
        }

    }

    private fun onFailureCall(message: String, errorCode: Int) {
        checkCode(errorCode)
        onFailureResponse(message, errorCode)

    }

    abstract fun onSuccessResponse(model: Model)

    abstract fun onFailureResponse(message: String, errorCode: Int)
}