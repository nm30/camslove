package retrofit.communications


import okhttp3.RequestBody
import retrofit.base.DataModel
import retrofit.datamodel.dashboard.*
import retrofit.datamodel.login.UserLoginResponse
import retrofit.datamodel.login.UserRegister
import retrofit.datamodel.loginsignup.*
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Android on 12/1/2016.
 */

interface ServerApis {

    companion object {
        private const val ACCESS_TOKEN = "api_key"

    }


    @POST("login")
    fun userLogin(
        @Body() data: UserRegister
    ): Call<DataModel<UserLoginResponse>>

    @POST("register_or_update")
    fun userRegister(
        @Body() data: UserRegister
    ): Call<DataModel<UserLoginResponse>>

    @POST("forgot_password")
    @FormUrlEncoded
    fun forgotPassword(
        @Field("email") email: String,
        @Header("Accept") acceptedDate: String = "application/json"
    ): Call<DataModel<UserLoginResponse>>

    @Multipart
    @POST("register_or_update")
    fun updateUserWithImageUpload(
        @Header(ACCESS_TOKEN) token: String,
        @Part()data: UserRegister,
        @Part("file\"; filename=\"default.png") imageFileBody: RequestBody?
    ): Call<DataModel<UserLoginResponse>>

    // User
    @POST("users/login")
    @FormUrlEncoded
    fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<LoginToken>

    @GET("247_languages")
    fun getLanguages(
        @Header(ACCESS_TOKEN) token: String?
    ): Call<List<Language>>

    @GET("247_pages")
    fun getPages(
        @Header(ACCESS_TOKEN) token: String?
    ): Call<List<Page>>

    @GET("247_static_pages")
    fun getStaticPages(
        @Header(ACCESS_TOKEN) token: String?
    ): Call<List<StaticPage>>

    @GET("247_strings")
    fun getStrings(
        @Header(ACCESS_TOKEN) token: String,
        @Query("language") languageId: String,
        @Query("page_id") pageId: Int
    ): Call<List<ScreenString>>

    @POST("users/register")
    @FormUrlEncoded
    fun userRegister(
        @Header(ACCESS_TOKEN) token: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<UserProfileResponse>

    @POST("users/update/{id}")
    @FormUrlEncoded
    fun userUpdate(
        @Path("id") id: Int,
        @Header(ACCESS_TOKEN) token: String,
        @Field("username") username: String,
        @Field("biz_role") bizRole: String,
        @Field("opponentID") opponentId: String
    ): Call<UserProfileResponse>

    @POST("247_users/update/{id}")
    @FormUrlEncoded
    fun userUpdateById(
        @Path("id") id: Int,
        @Header(ACCESS_TOKEN) token: String,
        @Field("first_name") firstName: String?,
        @Field("timeregistered") timeRegistered: String?,
        @Field("username") username: String?,
        @Field("password") password: String?,
        @Field("image") image: String?,
        @Field("email") email: String?,
        @Field("phone_number") phoneNumber: String?,
        @Field("last_name") lastName: String?,
        @Field("role") role: String?,
        @Field("solicitor_descprition") solicitorDescription: String?,
        @Field("solicitor_years") solicitorYears: Int?,
        @Field("solicitor_price") solicitorPrice: String?,
        @Field("solicitor_specialities") solicitorSpecialities: String?,
        @Field("solicitor_company") solicitorCompany: String?,
        @Field("solicitor_image") solicitorImage: String?,
        @Field("solicitor_status") solicitorStatus: String?,
        @Field("opponentID") opponentId: String?,
        @Field("company") company: String?
    ): Call<UserRole>

    @GET("247_users")
    fun getUsers(
        @Header(ACCESS_TOKEN) token: String,
        @Query("role") role: String?,
        @Query("id") id: Int?,
        @Query("first_name") firstName: String?,
        @Query("timeregistered") timeRegistered: String?,
        @Query("username") username: String?,
        @Query("password") password: String?,
        @Query("image") image: String?,
        @Query("email") email: String?,
        @Query("phone_number") phoneNumber: String?,
        @Query("last_name") lastName: String?,
        @Query("solicitor_descprition") solicitorDescription: String?,
        @Query("solicitor_years") solicitorYears: Int?,
        @Query("solicitor_price") solicitorPrice: String?,
        @Query("solicitor_specialities") solicitorSpecialities: String?,
        @Query("solicitor_company") solicitorCompany: String?,
        @Query("solicitor_image") solicitorImage: String?,
        @Query("solicitor_status") solicitorStatus: String?,
        @Query("opponentID") opponentId: String?,
        @Query("company") company: String?
    ): Call<List<UserRoleList>>

    @GET("247_users/{id}")
    fun getUserDetail(
        @Path("id") id: Int,
        @Header(ACCESS_TOKEN) token: String
    ): Call<UserRole>

    @Multipart
    @POST("files")
    fun uploadImageFile(
        @Header(ACCESS_TOKEN) token: String,
        @Part("file\"; filename=\"default.png") imageFileBody: RequestBody?
    ): Call<FileUploadRes>

    @GET("247_cases")
    fun getCases(
        @Header(ACCESS_TOKEN) token: String,
        @Query("solicitor_id") solicitorId: Int,
        @Query("user_id") userId: Int?
    ): Call<List<CasesList>>

    @GET("247_cases/{id}")
    fun getCaseDetail(
        @Path("id") id: Int,
        @Header(ACCESS_TOKEN) token: String
    ): Call<Case>

    @POST("247_cases")
    @FormUrlEncoded
    fun submitCase(
        @Header(ACCESS_TOKEN) token: String,
        @Field("solicitor_id") solicitorId: Int?,
        @Field("user_id") userId: Int?,
        @Field("title") title: String?,
        @Field("description") description: String?,
        @Field("document") document: String?,
        @Field("paid_date") paidDate: String?
    ): Call<Case>

    @POST("247_cases/update/{id}")
    @FormUrlEncoded
    fun updateCase(
        @Path("id") id: Int,
        @Header(ACCESS_TOKEN) token: String,
        @Field("solicitor_id") solicitorId: Int?,
        @Field("user_id") userId: Int?,
        @Field("title") title: String?,
        @Field("description") description: String?,
        @Field("document") document: String?,
        @Field("status") status: String?,
        @Field("paid_date") paidDate: String?,
        @Field("rejected_date") rejectedDate: String?,
        @Field("accepted_date") acceptedDate: String?,
        @Field("closed_date") closedDate: String?
    ): Call<Case>

    @GET("247_sessions")
    fun getSessions(
        @Header(ACCESS_TOKEN) token: String,
        @Query("solicitor_id") solicitorId: Int?,
        @Query("user_id") userId: Int?,
        @Query("token_session") tokenSession: String?,
        @Query("duration") duration: String?,
        @Query("case_id") caseId: Int?,
        @Query("opponentID") opponentId: String?,
        @Query("session_date") sessionDate: String?
    ): Call<List<SessionListRes>>

    @POST("247_sessions")
    @FormUrlEncoded
    fun submitSessions(
        @Header(ACCESS_TOKEN) token: String,
        @Field("solicitor_id") solicitorId: Int?,
        @Field("user_id") userId: Int?,
        @Field("token_session") tokenSession: String?,
        @Field("duration") duration: String?,
        @Field("case_id") caseId: Int?,
        @Field("opponentID") opponentId: String?,
        @Field("session_date") sessionDate: String?
    ): Call<SessionRes>

    @GET("247_filters")
    fun getFilters(
        @Header(ACCESS_TOKEN) token: String,
        @Field("id") id: Int? = null,
        @Field("name") name: String? = null,
        @Field("value") value: String? = null,
        @Field("role") role: String?
    ): Call<List<FilterRes>>

    @GET("247_solicitor_specialities")
    fun getSolicitorSpecialities(
        @Header(ACCESS_TOKEN) token: String,
        @Field("id") id: Int? = null,
        @Field("speciality") speciality: String? = null,
        @Field("language") language: String?
    ): Call<List<SolicitorSpecialitiesRes>>

    @GET("247_uk_counties")
    fun getUkCounties(
        @Header(ACCESS_TOKEN) token: String,
        @Field("int") id: Int? = null,
        @Field("code") code: String? = null,
        @Field("county") county: String? = null,
        @Field("value") value: String? = null
    ): Call<List<UkCounties>>


}