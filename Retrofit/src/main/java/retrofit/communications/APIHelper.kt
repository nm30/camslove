package retrofit.communications

import retrofit.datamodel.login.UserRegister
import retrofit.datamodel.loginsignup.UserProfileResponse


/**
 * Created by Amandeep Singh Bagli on 10/23/2017 @ 10:15 AM.
 */

interface APIHelper {


    fun userRegister(data: UserRegister)
    fun userLogin(data: UserRegister)
    fun forgotPassword(data: String)

    //  User
    fun loginToken(email: String, password: String)
    fun getLanguages(token: String)
    fun getPages(token: String)
    fun getStaticPages(token: String)
    fun getStrings(token: String, languageId: String, pageId: Int)
    fun userRegister(token: String, email: String, password: String)
    fun userUpdate(token: String, userId: Int, data: UserProfileResponse)
    fun getUsers(
        token: String, role: String?, id: Int?, firstName: String?, timeRegistered: String?,
        username: String?, password: String?, image: String?, email: String?, phoneNumber: String?,
        lastName: String?, solicitorDescription: String?, solicitorYears: Int?,
        solicitorPrice: String?, solicitorSpecialities: String?, solicitorCompany: String?,
        solicitorImage: String?, solicitorStatus: String?, opponentId: String?, company: String?
    )

    fun getUserDetail(token: String, userId: Int)
    fun getCases(token: String, solicitorId: Int, userId: Int?)
    fun getCaseDetail(token: String, caseId: Int)
    fun uploadFile(token: String, path: String)
    fun submitCase(
        token: String, solicitorId: Int?, userId: Int?, title: String?,
        description: String?, document: String?, paidDate: String?
    )

    fun updateCase(
        id: Int, token: String, solicitorId: Int?, userId: Int?, title: String?,
        description: String?, document: String?, status: String?, paidDate: String?,
        rejectedDate: String?, acceptedDate: String?, closedDate: String?
    )

    fun getSessions(
        token: String, solicitorId: Int?, userId: Int?, tokenSession: String?,
        duration: String?, caseId: Int?, opponentId: String?, sessionDate: String?
    )

    fun submitSessions(
        token: String, solicitorId: Int?, userId: Int?, tokenSession: String?,
        duration: String?, caseId: Int?, opponentId: String?, sessionDate: String?
    )

    fun userUpdateById(
        id: Int, token: String, firstName: String?, timeRegistered: String?,
        username: String?, password: String?, image: String?, email: String?,
        phoneNumber: String?, lastName: String?, role: String?, solicitorDescription: String?,
        solicitorYears: Int?, solicitorPrice: String?, solicitorSpecialities: String?,
        solicitorCompany: String?, solicitorImage: String?, solicitorStatus: String?,
        opponentId: String?, company: String?
    )

    fun getFilters(token: String, role: String?)
    fun getSolicitorSpecialities(token: String, languageId: String?)
    fun getUkCounties(token: String)

}