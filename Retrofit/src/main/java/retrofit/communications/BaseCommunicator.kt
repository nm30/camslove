package retrofit.communications

import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.preference.PreferenceManager
import androidx.annotation.UiThread
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity


import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit.RetrofitClient
import retrofit.contants.BaseKey
import retrofit.contants.BaseKey.Companion.KEY_ACCESS_TOKEN
import retrofit.contants.BaseKey.Companion.KEY_FCM_TOKEN
import retrofit.contants.BaseKey.Companion.KEY_TEMPRARY_TOKEN
import retrofit.contants.BaseKey.Companion.PROFILE_PIC
import retrofit.utilz.BaseUtil
import retrofit.utilz.Log
import java.io.File
import java.io.FileOutputStream


/**
 * Created by Amandeep Singh on 1/9/2017 11:00.
 */

abstract class BaseCommunicator<in CallbackAPI> @UiThread
protected constructor(callback: CallbackAPI) : BaseKey, APIHelper {

    protected val apiService: ServerApis
  //  protected val apiServiceJson: ServerApis
    private var preferences: SharedPreferences
    protected var TAG = javaClass.simpleName
    protected var activity: AppCompatActivity

    protected val preferencesEditor: SharedPreferences.Editor
        get() = getPreferences().edit()

    init {
        if (callback is AppCompatActivity) {
            activity = callback
        } else if (callback is Fragment) {
            activity = (callback as  Fragment).activity as AppCompatActivity
        } else {
            throw IllegalArgumentException("Call back must be implemented with activity or fragment class!!!")
        }
      //  apiService = RetrofitClient(activity).retrofitSSLModelBuilder().create(ServerApis::class.java)
      apiService = RetrofitClient(activity).retrofitModelBuilder().create(ServerApis::class.java)
    //    apiServiceJson = RetrofitClient(activity).retrofitBuilder().create(ServerApis::class.java)
        preferences = PreferenceManager.getDefaultSharedPreferences(activity)
    }

    protected fun getPreferences(): SharedPreferences {
        return preferences
    }

    protected fun <Value> saveData(key: String, value: Value) {

       // Paper.book().write(key, value)

    }

    protected fun <Value> getValue(key: String): Value? {

         return null
      //  return Paper.book().read(key);
    }

    protected fun delete(key: String) {
        // Paper.book().delete(key);
    }

    companion object {

        val pageSize = 1000
    }

    protected fun getAccessToken(): String? {

        return preferences.getString(KEY_ACCESS_TOKEN, null)

    }

    protected fun getTempraryToken(): String {

        return preferences.getString(KEY_TEMPRARY_TOKEN, null)

    }

    protected fun getFcmDeviceID(): String {

        return preferences.getString(KEY_FCM_TOKEN, "TOKEN_IS_NOT_AVAILABLE")
    }


    protected fun getProfilePic(): String? {
        return preferences.getString(PROFILE_PIC, null)
    }
    protected fun toRequestBody(value: String): RequestBody? {
        return if (BaseUtil.isNullOrEmpty(value)) null else RequestBody.create("text/plain".toMediaTypeOrNull(), value)
    }

    protected fun toImageRequest(imagePath: String?): RequestBody? {

        if (BaseUtil.isNullOrEmpty(imagePath))
            return null

        val file = getImageFile(imagePath) ?: return null
        // RequestBody fileBodyImage =RequestBody.create(MediaType.parse("image/png"), file);;

        return RequestBody.create("image/*".toMediaTypeOrNull(), file!!)
    }

    protected fun toImageRequestBody(imagePath: String?): RequestBody? {

        if (BaseUtil.isNullOrEmpty(imagePath))
            return null

        val file = getImageFile(imagePath) ?: return null

        // RequestBody fileBodyImage =RequestBody.create(MediaType.parse("image/png"), file);;

        return RequestBody.create("image/*".toMediaTypeOrNull(), file!!)
    }

    private fun getImageFile(filePath: String?): File? {
        if (filePath != null) {
            val bmp = BitmapFactory.decodeFile(filePath)
            val imageFile = File(filePath)
            var fOut: FileOutputStream? = null
            try {
                fOut = FileOutputStream(imageFile)
                bmp.compress(Bitmap.CompressFormat.JPEG, 75, fOut)
                fOut.flush()
                fOut.close()
            } catch (e: Exception) {
                Log.e(TAG, "Bitmap.CompressFormat.JPEG/FileOutputStream:ParcelDetail", e)
            }

            return imageFile
        }
        return null
    }

    protected fun toImageRequestBody(imagePath: Uri): RequestBody? {
        var file: File? = null
        try {
            file = File(imagePath.toString().replace("content", "file")
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return if (file == null) null else RequestBody.create("image/*".toMediaTypeOrNull(), file)

    }

}
