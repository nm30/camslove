package retrofit.communications

import retrofit.BaseResponseResolver
import retrofit.ListResponseResolver
import retrofit.ResponseResolver
import retrofit.ResponseType
import retrofit.base.APICallback
import retrofit.base.BaseData
import retrofit.base.DataModel
import retrofit.base.ListModel
import retrofit.datamodel.dashboard.*
import retrofit.datamodel.login.UserLoginResponse
import retrofit.datamodel.login.UserRegister
import retrofit.datamodel.loginsignup.*
import retrofit.utilz.BaseUtil
import retrofit.utilz.Log


/**
 * Created by Amandeep Singh Bagli on 10/23/2017 @ 10:01 AM.
 */
open class APICall<in CallBackAPI : APICallback<BaseData>> private constructor(
    callBackAPI:
    CallBackAPI
) :
    BaseCommunicator<CallBackAPI>(callBackAPI) {

    private val callBackAPI: APICallback<BaseData> = callBackAPI
    private val limit = 10

    companion object {
        fun build(callback: APICallback<BaseData>): APIHelper {
            return APICall(callback)
        }
    }

    override fun userLogin(data: UserRegister) {
        val apiCall = apiService.userLogin(data)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel(data)}")
        apiCall.enqueue(object : ResponseResolver<DataModel<UserLoginResponse>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: DataModel<UserLoginResponse>) {
                model.setResponseType(ResponseType.USER_LOGIN)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun userRegister(data: UserRegister) {
        val apiCall = apiService.userRegister(data)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel(data)}")
        apiCall.enqueue(object : ResponseResolver<DataModel<UserLoginResponse>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model:DataModel< UserLoginResponse>) {
                model.setResponseType(ResponseType.USER_REGISTER)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun forgotPassword(data: String) {
        val apiCall = apiService.forgotPassword(data)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel(data)}")
        apiCall.enqueue(object : ResponseResolver<DataModel<UserLoginResponse>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: DataModel<UserLoginResponse>) {
                model.setResponseType(ResponseType.FORGOT_PASSWORD)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun loginToken(email: String, password: String) {
        val apiCall = apiService.login(email, password)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$email\n$password")}")
        apiCall.enqueue(object : ResponseResolver<LoginToken,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: LoginToken) {
                model.setResponseType(ResponseType.LOGIN_TOKEN)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getLanguages(token: String) {
        val apiCall = apiService.getLanguages(token)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel(token)}")
        apiCall.enqueue(object : ListResponseResolver<List<Language>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_LANGUAGES)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getPages(token: String) {
        val apiCall = apiService.getPages(token)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel(token)}")
        apiCall.enqueue(object : ListResponseResolver<List<Page>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_PAGES)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getStaticPages(token: String) {
        val apiCall = apiService.getStaticPages(token)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel(token)}")
        apiCall.enqueue(object : ListResponseResolver<List<StaticPage>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_STATIC_PAGES)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getStrings(token: String, languageId: String, pageId: Int) {
        val apiCall = apiService.getStrings(token, languageId, pageId)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$token\n$languageId\n$pageId")}")
        apiCall.enqueue(object : ListResponseResolver<List<ScreenString>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_STRINGS)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun userRegister(token: String, email: String, password: String) {
        val apiCall = apiService.userRegister(token, email, password)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$token\n$email\n$password")}")
        apiCall.enqueue(object : ResponseResolver<UserProfileResponse,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: UserProfileResponse) {
                model.setResponseType(ResponseType.USER_REGISTER)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun userUpdate(token: String, userId: Int, data: UserProfileResponse) {
        val username = data.username ?: ""
        val bizRole = data.bizRole ?: ""
        val opponentId = data.opponentId ?: ""
        val apiCall = apiService.userUpdate(userId, token, username, bizRole, opponentId)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$userId\n$token\n$data")}")
        apiCall.enqueue(object : ResponseResolver<UserProfileResponse,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: UserProfileResponse) {
                model.setResponseType(ResponseType.USER_UPDATE)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getUsers(
        token: String, role: String?, id: Int?, firstName: String?, timeRegistered: String?,
        username: String?, password: String?, image: String?, email: String?, phoneNumber: String?,
        lastName: String?, solicitorDescription: String?, solicitorYears: Int?,
        solicitorPrice: String?, solicitorSpecialities: String?, solicitorCompany: String?,
        solicitorImage: String?, solicitorStatus: String?, opponentId: String?, company: String?
    ) {
        val apiCall = apiService.getUsers(
            token, role, id, firstName, timeRegistered, username, password, image, email,
            phoneNumber, lastName, solicitorDescription, solicitorYears, solicitorPrice,
            solicitorSpecialities, solicitorCompany, solicitorImage, solicitorStatus,
            opponentId, company
        )
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$token\n$role")}")
        apiCall.enqueue(object : ListResponseResolver<List<UserRoleList>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_USERS)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getUserDetail(token: String, userId: Int) {
        val apiCall = apiService.getUserDetail(userId, token)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$userId\n$token")}")
        apiCall.enqueue(object : ResponseResolver<UserRole,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: UserRole) {
                model.setResponseType(ResponseType.GET_USER_DETAIL)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getCases(token: String, solicitorId: Int, userId: Int?) {
        val apiCall = apiService.getCases(token, solicitorId, userId)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$token\n$solicitorId")}")
        apiCall.enqueue(object : ListResponseResolver<List<CasesList>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_CASES)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getCaseDetail(token: String, caseId: Int) {
        val apiCall = apiService.getCaseDetail(caseId, token)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$caseId\n$token")}")
        apiCall.enqueue(object : ResponseResolver<Case,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: Case) {
                model.setResponseType(ResponseType.GET_CASE_DETAIL)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun uploadFile(token: String, path: String) {
        val apiCall = apiService.uploadImageFile(token, toImageRequest(path))
        // Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$token\n$path")}")
        apiCall.enqueue(object : ResponseResolver<FileUploadRes,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: FileUploadRes) {
                model.setResponseType(ResponseType.UPLOAD_FILE)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun submitCase(
        token: String, solicitorId: Int?, userId: Int?, title: String?,
        description: String?, document: String?, paidDate: String?
    ) {
        val apiCall = apiService.submitCase(
            token, solicitorId, userId, title, description, document, paidDate
        )
        Log.e(
            TAG, "-----Data----- ${
                BaseUtil.jsonFromModel(
                    "$token\n$solicitorId\n" + "$userId\n" +
                            "$description\n" + "$document\n" + "$paidDate"
                )
            }"
        )
        apiCall.enqueue(object : ResponseResolver<Case,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: Case) {
                model.setResponseType(ResponseType.SUBMIT_CASE)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun updateCase(
        id: Int, token: String, solicitorId: Int?, userId: Int?, title: String?,
        description: String?, document: String?, status: String?, paidDate: String?,
        rejectedDate: String?, acceptedDate: String?, closedDate: String?
    ) {
        val apiCall = apiService.updateCase(
            id, token, solicitorId, userId, title, description, document, status, paidDate,
            rejectedDate, acceptedDate, closedDate
        )
        Log.e(
            TAG, "-----Data----- ${
                BaseUtil.jsonFromModel(
                    "$id\n$token\n$solicitorId\n" + "$userId\n" +
                            "$description\n" + "$document\n" + "$paidDate"
                )
            }"
        )
        apiCall.enqueue(object : ResponseResolver<Case,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: Case) {
                model.setResponseType(ResponseType.UPDATE_CASE)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getSessions(
        token: String, solicitorId: Int?, userId: Int?, tokenSession: String?,
        duration: String?, caseId: Int?, opponentId: String?, sessionDate: String?
    ) {
        val apiCall = apiService.getSessions(
            token, solicitorId, userId, tokenSession, duration, caseId, opponentId, sessionDate
        )
        Log.e(
            TAG, "-----Data----- ${
                BaseUtil.jsonFromModel(
                    "$token\n$solicitorId\n" + "$userId\n" + "$tokenSession\n" + "$duration\n" +
                            "$caseId" + "$opponentId\n" + "$sessionDate"
                )
            }"
        )
        apiCall.enqueue(object : ListResponseResolver<List<SessionListRes>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_SESSION)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun submitSessions(
        token: String, solicitorId: Int?, userId: Int?, tokenSession: String?,
        duration: String?, caseId: Int?, opponentId: String?, sessionDate: String?
    ) {
        val apiCall = apiService.submitSessions(
            token, solicitorId, userId, tokenSession, duration, caseId, opponentId, sessionDate
        )
        Log.e(
            TAG, "-----Data----- ${
                BaseUtil.jsonFromModel(
                    "$token\n$solicitorId\n" + "$userId\n" + "$tokenSession\n" + "$duration\n" +
                            "$caseId" + "$opponentId\n" + "$sessionDate"
                )
            }"
        )
        apiCall.enqueue(object : ResponseResolver<SessionRes,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: SessionRes) {
                model.setResponseType(ResponseType.SUBMIT_SESSION)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun userUpdateById(
        id: Int, token: String, firstName: String?, timeRegistered: String?, username: String?,
        password: String?, image: String?, email: String?, phoneNumber: String?, lastName: String?,
        role: String?, solicitorDescription: String?, solicitorYears: Int?, solicitorPrice: String?,
        solicitorSpecialities: String?, solicitorCompany: String?, solicitorImage: String?,
        solicitorStatus: String?, opponentId: String?, company: String?
    ) {
        val apiCall = apiService.userUpdateById(
            id, token, firstName, timeRegistered, username, password, image, email,
            phoneNumber, lastName, role, solicitorDescription, solicitorYears, solicitorPrice,
            solicitorSpecialities, solicitorCompany, solicitorImage, solicitorStatus,
            opponentId, company
        )
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$id\n$token\n$token")}")
        apiCall.enqueue(object : ResponseResolver<UserRole,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: UserRole) {
                model.setResponseType(ResponseType.USER_UPDATE_BY_ID)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getFilters(token: String, role: String?) {
        val apiCall = apiService.getFilters(token, null, null, null, role)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$token\n$role")}")
        apiCall.enqueue(object : ListResponseResolver<List<FilterRes>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_FILTERS)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getSolicitorSpecialities(token: String, languageId: String?) {
        val apiCall = apiService.getSolicitorSpecialities(token, null, null, languageId)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel("$token\n$languageId")}")
        apiCall.enqueue(object : ListResponseResolver<List<SolicitorSpecialitiesRes>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_SOLICITOR_SPECIALITIES)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

    override fun getUkCounties(token: String) {
        val apiCall = apiService.getUkCounties(token, null, null, null, null)
        Log.e(TAG, "-----Data----- ${BaseUtil.jsonFromModel(token)}")
        apiCall.enqueue(object : ListResponseResolver<List<UkCounties>,
                APICallback<BaseData>>(callBackAPI) {

            override fun onSuccessResponse(model: ListModel<*>) {
                model.setResponseType(ResponseType.GET_UK_COUNTIES)
                Log.e(TAG, "-----Response----- ${BaseUtil.jsonFromModel(model)}")
                //To change body of created functions use File | Settings | File Templates.
                callBackAPI.onSuccessfullyCallModel(model)
            }

            override fun onFailureResponse(message: String, errorCode: Int) {
                Log.e(TAG, message)
                callBackAPI.onFailureCall(message, errorCode)
            }

        })
    }

}