package retrofit.base

import retrofit.ResponseType


/**
 * Created by Amandeep Singh Bagli on 10/23/2017 @ 10:03 AM.
 */
interface APICallback<in Data : BaseData> {
    fun onSuccessfullyCallModel(response: BaseResponse<Data>)
    fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType = ResponseType.COMMON)
}