package retrofit.base

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Amandeep Singh Bagli on 10/23/2017 @ 3:23 PM.
 */

open class DataModel<Data : BaseData> : BaseModel<Data>() {
    @SerializedName("data")
    @Expose
    private val data: Data? = null

    override fun getData(): Data? {
        return data
    }
}
