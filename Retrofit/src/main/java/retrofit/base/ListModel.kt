package retrofit.base



import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Amandeep Singh Bagli on 10/22/2017 @ 4:34 PM.
 */

open class ListModel<Data: BaseData> : BaseModel<Data> {

    @SerializedName("data")
    @Expose
    private var data: List<Data>? = null

    override fun getListData(): List<Data>? {
        return data
    }

    constructor():super(){
    }

    constructor(message:String,responseCode: Int):super(message,responseCode){

    }
    constructor(message:String,responseCode: Int,data: List<Data>):super(message,responseCode){

        this.data=data
    }
}
