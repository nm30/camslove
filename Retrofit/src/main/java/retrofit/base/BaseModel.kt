package retrofit.base


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import retrofit.ResponseType


/**
 * Created by Amandeep Singh on 1/9/2017 11:59.
 */

open class BaseModel<out Data : BaseData> : BaseResponse<Data> {
    @SerializedName("code")
    @Expose
    private var responseCode: Int = 0
    @SerializedName("is_success")
    @Expose
    private val success: Boolean = false
    @SerializedName("message")
    @Expose
    private var message: String? = null

    private var responseType: ResponseType = ResponseType.COMMON


    constructor(){

    }
    constructor(message:String,responseCode: Int){
        this.message=message
        this.responseCode=responseCode
    }

    override fun toString(): String {
        return super.toString() + "Message $message  => ResponseCode $responseCode  => ResponseType $responseType"
    }


    // keytool -exportcert -alias android -keystore C:\Users\IT\Downloads\Skype\bands.keystore | "F:\02152017\OpenSSL\bin\openssl.exe" sha1 -binary | "F:\02152017\OpenSSL\bin\openssl.exe" base64 OPENSSL_CONF=F:\02152017\OpenSSL\bin\openssl.cfg
    //rp3vhywy7zsbo7SQoYzNbKshFl8=
    override fun getResponseCode(): Int {
        //("not implemented") //To change body of created functions use File | Settings | File Templates.
        return responseCode
    }

    override fun getMessage(): String {
        //("not implemented") //To change body of created functions use File | Settings | File Templates.

        if (message == null)
            return "Something went wrong"
        return message!!
    }

    override fun getData(): Data? {
        //("not implemented") //To change body of created functions use File | Settings | File Templates.
        return null
    }

    override fun getListData(): List<Data>? {
        //("not implemented") //To change body of created functions use File | Settings | File Templates.

        return null
    }

    fun getBaseResponse(): BaseResponse<Data> {
        return this
    }

    override fun getResponseType(): ResponseType {
        return responseType


    }

    fun setResponseType(responseType: ResponseType) {
        this.responseType = responseType
    }

    fun updateResponseCode(responseCode: Int) {
        this.responseCode = responseCode;
    }
}
