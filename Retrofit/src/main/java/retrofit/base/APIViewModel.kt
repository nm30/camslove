package retrofit.base


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel




class APIViewModel : ViewModel() {

     internal  var baseData: MutableLiveData< BaseModel<BaseData>> = MutableLiveData<BaseModel<BaseData>>()

  public fun getBaseData(): MutableLiveData<BaseModel<BaseData>> {
        if (baseData == null) {
            baseData = MutableLiveData<BaseModel<BaseData>>()
        }
        return baseData
    }


}
