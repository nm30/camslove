package retrofit.base

import retrofit.ResponseType


/**
 * Created by Amandeep Singh Bagli on 10/22/2017 @ 4:30 PM.
 */

interface BaseResponse<out Data : BaseData> {

    fun getResponseCode(): Int

    fun getMessage(): String?

    fun getResponseType(): ResponseType

    fun getData(): Data?

    fun getListData(): List<Data>?
}
