package retrofit.contants;

public interface SocketKey {

    String KEY_TOKEN="token";
    String EVENT_AUTHENTICATED="authenticated";
    String EVENT_AUTHENTICATE="authenticate";
    String EVENT_COOKING_RESULT="cookingResult";
    String EVENT_START_COOKING_RECIPE="startCookingRecipe";
    String EVENT_COOKING_DETAILS="cookingDetails";
    String EVENT_GET_COOKING_DETAILS="getCookingDetails";
    String EVENT_RESULT_COOKING_LIVE_DATA="result";
    String EVENT_ON_RECIPE_CHANGES_DONE="recipeChangesDone";
    String EVENT_GET_KITCHEN_RECIPES_INFO="getKitchenRecipesInfo";
    String EVENT_GET_KITCHEN_RECIPES_INFO_RESULT="result";
    String EVENT_GET_KITCHEN_RECIPES_INFO_CHANGES="changesDone";
}
