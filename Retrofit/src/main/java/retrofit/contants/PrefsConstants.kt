package retrofit.contants

class PrefsConstants {
    companion object {
        val DEFAULT = "DEFAULT"
        val LOGIN_TOKEN = "LOGIN_TOKEN"
        val FIREBASE_TOKEN = "FIREBASE_TOKEN"
        val USER_DETAILS = "USER_DETAILS"
        val APP_LANGUAGE_ID = "APP_LANGUAGE_ID"
        val SELECTED_LANGUAGE = "SELECTED_LANGUAGE"
        val IS_SELECTED_LANGUAGE = "IS_SELECTED_LANGUAGE"
        val IS_LOGIN = "IS_LOGIN"
        val ACCESS_TOKEN = "ACCESS_TOKEN"
        val LOGIN_TYPE = "LOGIN_TYPE"
        val USER_ROLE = "USER_ROLE"
        val USER_PROFILE = "USER_PROFILE"
        val USER_REGISTER_SUCCESS = "USER_REGISTER_SUCCESS"

        val PRIVACY_POLICY_URL = "https://laravel.appmantechnologies.com/camlove/policy"
        val TERMS_AND_CONDITION_URL = "https://laravel.appmantechnologies.com/camlove/terms"

        val CREATE_PROFILE = 101
        val ACCOUNT_SETTINGS = 102
        val PRIVACY_POLICY = 103
        val TERMS_AND_CONDITION = 104
        val ABOUT_US = 105

        val GENDER_MALE=1
        val GENDER_FEMALE=2
    }

}