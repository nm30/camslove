package retrofit.contants

/**
 * Created by Amandeep Singh on 1/9/2017 17:05.
 */

public interface BaseKey {
    companion object {

        const val KEY_ACCESS_TOKEN = "x-access-token-KEY"
        val KEY_TEMPRARY_TOKEN = "temporaryToken"
        val PROFILE_PIC = "profilePic"
        val KEY_FCM_TOKEN = "com.event.finder.fcm.MyFirebaseInstanceIDService.KEY"

    }

}
