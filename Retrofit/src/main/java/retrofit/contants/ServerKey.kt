package retrofit.contants

public interface ServerKey {
    companion object {
        val KEY_OWNER = "Owner"
        val KEY_CREATOR = "Creator"
        val KEY_SUPER_USER = "Manager"
        val KEY_WORKER = "Worker"
        val KEY_ADDMANAGER = "Add Super User"
        val KEY_ADDUSER = "AddUser"
        val KEY_EQUIPMENTLIST = "EquipmentList"
        val KEY_PACKAGETYPE = "PackageType"
        val KEY_SUBSCRIPTION_NEW = "New"
        val KEY_SUBSCRIPTION_OLD = "Old"
        val KEY_SUBSCRIPTION_FreeTrial = "FreeTrial"
        val KEY_USER = "User"
        val KEY_RECIPELIST = "RecipeList"
        val KEY_CURRENTWORKSTATUS = "CurrentWorkStatus"
        val KEY_EMPLOYEESTATS = "Employee Statistic Page"
        val KEY_EMPLOYEE_REPORT = "Employee Reports"
        val KEY_PROFILE_INFO = "OwnerProfile"
        val KEY_ZONELIST = "ZoneList"
        val KEY_OVERCOOK = "OverCook"
        val KEY_LIGHTCOOK = "LightCook"
        val KEY_FACLITATOR = "Facilitator"
        val KEY_SUPER_USER_LOGIN = "Super User login"
        val KEY_ADD_LOCATION = "Add location"

        val VALUE_TOKEN_EXPIRED = 403
    }
}