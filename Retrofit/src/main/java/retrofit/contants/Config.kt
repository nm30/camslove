package retrofit.contants

/**
 * Created by Amandeep Singh on 1/27/2017 13:35.
 */

object Config {

    private val socketMODE = Server.TEST
    private val RECORDS_LIMIT = 10
    private val RECORDS_LIMIT_FOR_WORKERS = 3
    val baseUrl = "${getSocketURL()}/"

    val versionCode = "202"
    //val versionCode = "103"
    val deviceType = "android"

    fun getRecordsLimit(): Int {
        return RECORDS_LIMIT
    }

    private fun getSocketURL() = socketMODE.url

    fun getWorkersRecordsLimit(): Int {
        return RECORDS_LIMIT_FOR_WORKERS
    }

    private enum class Server constructor(val url: String) {
        TEST("https://laravel.appmantechnologies.com/camlove/api"),
        LIVE("https://laravel.appmantechnologies.com/camlove/api"),
        IP("https://laravel.appmantechnologies.com/camlove/api")

    }

}
