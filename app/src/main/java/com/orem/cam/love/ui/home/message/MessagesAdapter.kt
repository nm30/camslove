package com.orem.cam.love.ui.home.message

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseAdapter
import retrofit.datamodel.dashboard.UserRoleList
import retrofit.utilz.Log

/**
 * Created by Manish Bhargav
 */

class MessagesAdapter(private val callback: Callback) :
    BaseAdapter<MessagesAdapter.ViewHolder>() {

    private val items = ArrayList<UserRoleList>()
    private var selectedIndex = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(
            R.layout.item_messages, parent, false
        )
        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(v, callback)

    }

    override fun getItemCount(): Int {
        Log.e(TAG, "-------- ${items.size}")
        return 4
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val item = items[position]
//        holder.bind(item, position)
    }

    inner class ViewHolder(itemView: View, private val callback: Callback) :
        RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                callback.onClickAdapter(adapterPosition)
                notifyDataSetChanged()
            }
        }

        fun bind(data: UserRoleList, index: Int) {
        }
    }

    fun setItems(list: List<UserRoleList>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun addItems(newItems: List<UserRoleList>) {
        val oldListSize = this.items.size
        this.items.addAll(newItems)
        notifyItemRangeInserted(oldListSize, newItems.size)
    }

    fun getItems(position: Int): UserRoleList {
        return items[position]
    }

    interface Callback {
        fun onClickAdapter(position: Int)
    }
}