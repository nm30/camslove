package com.orem.cam.love.ui.home

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseFragment
import com.orem.cam.love.databinding.FragmentHomeBinding
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse

class HomeFragment : BaseFragment(), HomeAdapter.Callback {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var adapter: HomeAdapter

    override fun getFragmentLayoutResId() = R.layout.fragment_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.bind(view)!!
        inIt()
        inItListener()
    }

    private fun inIt() {
        adapter = HomeAdapter(this)
        binding.rvHome.adapter = adapter
    }

    private fun inItListener() {

    }


    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

    override fun onClickAdapter(position: Int) {

    }
}