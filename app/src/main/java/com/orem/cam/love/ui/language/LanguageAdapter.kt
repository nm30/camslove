package com.orem.cam.love.ui.language

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseAdapter
import com.orem.cam.love.databinding.ItemLanguageBinding
import retrofit.model.Language
import retrofit.utilz.Log

/**
 * Created by Manish Bhargav
 */

class LanguageAdapter(private val callback: Callback) :
    BaseAdapter<LanguageAdapter.ViewHolder>() {

    private val items = ArrayList<Language>()
    private var selectedIndex = 0
    private lateinit var binding: ItemLanguageBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_language, parent, false
        )
        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(binding.root, callback)

    }

    override fun getItemCount(): Int {
        Log.e(TAG, "-------- ${items.size}")
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item, position)
    }

    inner class ViewHolder(itemView: View, private val callback: Callback) :
        RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                callback.onClickAdapter(adapterPosition)
                notifyDataSetChanged()
            }
        }

        fun bind(data: Language, index: Int) {
            binding.tvItemLanguage.text = data.title
            binding.ivItemFlag.setImageDrawable(data.url?.let {
                ContextCompat.getDrawable(
                    itemView.context, it
                )
            })
            if (data.isSelected == true) {
                binding.clLanguage.background = ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.bg_rectangle_corner_gender_selected
                )
            } else {
                binding.clLanguage.background = ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.bg_rectangle_corner_gender_un_selected
                )
            }
        }
    }

    fun setItems(list: List<Language>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun addItems(newItems: List<Language>) {
        val oldListSize = this.items.size
        this.items.addAll(newItems)
        notifyItemRangeInserted(oldListSize, newItems.size)
    }

    fun getItems(position: Int): Language {
        return items[position]
    }

    interface Callback {
        fun onClickAdapter(position: Int)
    }
}