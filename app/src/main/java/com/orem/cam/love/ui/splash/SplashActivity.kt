package com.orem.cam.love.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.ui.home.HomeActivity
import com.orem.cam.love.ui.language.SelectLanguageActivity
import com.orem.cam.love.ui.signUp.SignUpTypeActivity
import com.orem.cam.love.utils.PrefsManager
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.contants.PrefsConstants

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setHandler()
    }


    private fun setHandler() {
        Handler(Looper.getMainLooper()).postDelayed({
            when {
                PrefsManager.get().getBoolean(PrefsConstants.IS_LOGIN, false) -> {
                    navigateToHome()
                }
                PrefsManager.get().getBoolean(PrefsConstants.IS_SELECTED_LANGUAGE, false) -> {
                    navigateToStart()
                }
                else -> {
                    navigateToSelectLanguage()
                }
            }
        }, 2000)
    }

    private fun navigateToSelectLanguage() {
        val intent = Intent(this, SelectLanguageActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun navigateToStart() {
        val intent = Intent(this, SignUpTypeActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun navigateToHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }
}