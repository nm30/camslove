package com.orem.cam.love.ui.rating

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityReviewBinding
import com.orem.cam.love.ui.home.HomeActivity
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse

class ReviewActivity : BaseActivity() {

    private lateinit var binding: ActivityReviewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_review)

        inItListener()
    }

    private fun inItListener() {
        binding.ivClose.setOnClickListener { navigate() }
        binding.btnDone.setOnClickListener { navigate() }
    }

    private fun navigate() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }


    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }
}