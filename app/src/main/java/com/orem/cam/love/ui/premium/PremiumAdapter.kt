package com.orem.cam.love.ui.premium

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseAdapter
import com.orem.cam.love.databinding.ItemPremiumPlansBinding
import retrofit.model.Language
import retrofit.utilz.Log

/**
 * Created by Manish Bhargav
 */

class PremiumAdapter(private val callback: Callback) :
    BaseAdapter<PremiumAdapter.ViewHolder>() {

    private val items = ArrayList<Language>()
    private var selectedIndex = 0
    private lateinit var binding: ItemPremiumPlansBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_premium_plans, parent, false
        )
        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(binding.root, callback)

    }

    override fun getItemCount(): Int {
        Log.e(TAG, "-------- ${items.size}")
        return 3
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val item = items[position]
//        holder.bind(item, position)
    }

    inner class ViewHolder(itemView: View, private val callback: Callback) :
        RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                callback.onClickAdapter(adapterPosition)
                notifyDataSetChanged()
            }
        }

        fun bind(data: Language, index: Int) {}
    }

    fun setItems(list: List<Language>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun addItems(newItems: List<Language>) {
        val oldListSize = this.items.size
        this.items.addAll(newItems)
        notifyItemRangeInserted(oldListSize, newItems.size)
    }

    fun getItems(position: Int): Language {
        return items[position]
    }

    interface Callback {
        fun onClickAdapter(position: Int)
    }
}