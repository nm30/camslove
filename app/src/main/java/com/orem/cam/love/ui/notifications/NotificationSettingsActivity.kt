package com.orem.cam.love.ui.notifications

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityHelpAndSupportBinding
import com.orem.cam.love.databinding.ActivityNotificationSettingsBinding
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.model.NotificationAndSupport

class NotificationSettingsActivity : BaseActivity(), NotificationSettingsAdapter.Callback {

    private lateinit var binding: ActivityNotificationSettingsBinding
    private lateinit var adapter: NotificationSettingsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_settings)
        inIt()
        inItListener()
    }

    private fun inIt() {
        adapter = NotificationSettingsAdapter(this)
        binding.rvNotificationSettings.adapter = adapter
        adapter.setItems(getList())
    }

    private fun getList(): ArrayList<NotificationAndSupport> {
        val list = arrayListOf<NotificationAndSupport>()
        list.add(
            NotificationAndSupport(
                id = "1", title = getString(R.string.notification_email_title),
                description = getString(R.string.notification_email_message)
            )
        )
        list.add(
            NotificationAndSupport(
                id = "2", title = getString(R.string.notification_new_title),
                description = getString(R.string.notification_new_message)
            )
        )
        list.add(
            NotificationAndSupport(
                id = "3", title = getString(R.string.notification_added_friend_title),
                description = getString(R.string.notification_added_friend_message)
            )
        )
        return list
    }

    private fun inItListener() {
        binding.ivBack.setOnClickListener { onBackPressed() }
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

    override fun onClickAdapter(position: Int) {

    }
}