package com.orem.cam.love.ui.signUp

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivitySignUpBinding
import com.orem.cam.love.ui.home.HomeActivity
import com.orem.cam.love.ui.login.LoginActivity
import com.orem.cam.love.utils.PrefsManager
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.contants.Config
import retrofit.contants.PrefsConstants
import retrofit.datamodel.login.UserLoginResponse
import retrofit.datamodel.login.UserRegister
import retrofit.utilz.BaseUtil
import retrofit.utilz.Log

class SignUpActivity : BaseActivity() {

    private lateinit var binding: ActivitySignUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        inIt()
        inItListener()
    }

    private fun inIt() {

    }

    private fun inItListener() {
        binding.ivBack.setOnClickListener { onBackPressed() }
        binding.tvLogin.setOnClickListener { navigate() }
        binding.btnRegister.setOnClickListener { navigateCreateAccount() }
    }

    private fun navigate() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun navigateHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    private fun callApi(data: UserRegister) {
        if (isNetworkAvailableWithMessage()) {
            progressDialog.showLoadingDialog(this, null)
            apiHelper.userRegister(data)
        }
    }

    private fun navigateCreateAccount() {
//        val intent = ProfileActivity.getProfileIntent(this, PrefsConstants.CREATE_PROFILE)
//        startActivity(intent)

        if (!validation()) return
        callApi(
            UserRegister(
                firstName = binding.edtFirst.text.toString(),
                lastName = binding.edtLast.text.toString(),
                email = binding.edtEmail.text.toString(),
                password = binding.edtPassword.text.toString(),
                deviceType = Config.deviceType,
                deviceToken = PrefsManager.get()
                    .getString(PrefsConstants.FIREBASE_TOKEN, getString(R.string.default_value)),
                countryCode = binding.tvCountryCode.selectedCountryCode?.replace("+", ""),
                phone = binding.edtPhoneNumber.text.toString(),
                signupType = 1,
                userType = 2
            )
        )
    }

    private fun validation(): Boolean {
        var isValid = true
        if (binding.edtFirst.text.toString().isEmpty()) {
            binding.edtFirst.error = getString(R.string.error_empty_first_name)
            isValid = false
        }
        if (binding.edtLast.text.toString().isEmpty()) {
            binding.edtLast.error = getString(R.string.error_empty_first_name)
            isValid = false
        }
        if (binding.edtEmail.text.toString().isEmpty() ||
            !Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text.toString()).matches()
        ) {
            binding.edtEmail.error = getString(R.string.error_empty_email)
            isValid = false
        }
        if (binding.edtPassword.text.toString().isEmpty()) {
            binding.edtPassword.error = getString(R.string.error_empty_password)
            isValid = false
        }
        if (binding.edtConfirmPassword.text.toString().isEmpty()) {
            binding.edtConfirmPassword.error = getString(R.string.error_empty_confirm_password)
            isValid = false
        }
        if (binding.edtPassword.text.toString() != binding.edtConfirmPassword.text.toString()
        ) {
            binding.edtConfirmPassword.error = getString(R.string.error_empty_confirm_password)
            isValid = false
        }
        if (binding.edtPhoneNumber.text.toString().isEmpty()) {
            binding.edtPhoneNumber.error = getString(R.string.error_empty_phone_number)
            isValid = false
        }
        return isValid
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        when (response.getResponseType()) {
            ResponseType.USER_REGISTER -> {
                progressDialog.dismissLoadingDialog()
                val data = response.getData() as UserLoginResponse
                Log.d(TAG, " response = ${BaseUtil.jsonFromModel(data)}")
//                shortSnackBar(response.getMessage() ?: "", binding.root)
                shortToast(response.getMessage() ?: "")
                navigateHome()
            }

        }
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        progressDialog.dismissLoadingDialog()
        Log.d(TAG, "$message, $errorCode, $responseType")
        shortSnackBar(message, binding.root)
    }
}