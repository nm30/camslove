package com.orem.cam.love.ui.helpSupport

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseAdapter
import com.orem.cam.love.databinding.ItemHelpAndSupportBinding
import retrofit.model.NotificationAndSupport
import retrofit.utilz.Log

/**
 * Created by Manish Bhargav
 */

class HelpAndSupportAdapter(private val callback: Callback) :
    BaseAdapter<HelpAndSupportAdapter.ViewHolder>() {

    private val items = ArrayList<NotificationAndSupport>()
    private var selectedIndex = 0
    private lateinit var binding: ItemHelpAndSupportBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_help_and_support, parent, false
        )
        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(binding.root, callback)

    }

    override fun getItemCount(): Int {
        Log.e(TAG, "-------- ${items.size}")
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item, position)
    }

    inner class ViewHolder(itemView: View, private val callback: Callback) :
        RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                callback.onClickAdapter(adapterPosition)
                notifyDataSetChanged()
            }
        }

        fun bind(data: NotificationAndSupport, index: Int) {
            binding.tvItemName.text = data.title
            binding.tvItemMessage.text = data.description
        }
    }

    fun setItems(list: List<NotificationAndSupport>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun addItems(newItems: List<NotificationAndSupport>) {
        val oldListSize = this.items.size
        this.items.addAll(newItems)
        notifyItemRangeInserted(oldListSize, newItems.size)
    }

    fun getItems(position: Int): NotificationAndSupport {
        return items[position]
    }

    interface Callback {
        fun onClickAdapter(position: Int)
    }
}