package com.orem.cam.love.ui.home

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityHomeBinding
import com.orem.cam.love.ui.home.message.MessagesFragment
import com.orem.cam.love.ui.home.search.SearchFragment
import com.orem.cam.love.ui.home.settings.SettingsFragment
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse

class HomeActivity : BaseActivity() {

    private lateinit var binding: ActivityHomeBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        if (savedInstanceState == null) {
            val fragment = HomeFragment()
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_home, fragment, fragment.javaClass.simpleName)
                .commit()
        }
        inItListener()
    }

    private fun inItListener() {
        binding.bnvHome.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    val fragment = HomeFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container_home, fragment, fragment.javaClass.simpleName)
                        .commit()
                }
                R.id.search -> {
                    val fragment = SearchFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container_home, fragment, fragment.javaClass.simpleName)
                        .commit()
                }
                R.id.message -> {
                    val fragment = MessagesFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container_home, fragment, fragment.javaClass.simpleName)
                        .commit()
                }
                R.id.settings -> {
                    val fragment = SettingsFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container_home, fragment, fragment.javaClass.simpleName)
                        .commit()
                }
            }
            true
        }
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }
}