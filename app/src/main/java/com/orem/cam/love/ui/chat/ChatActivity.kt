package com.orem.cam.love.ui.chat

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.PopupMenu
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityChatBinding
import com.orem.cam.love.databinding.DialogOverAgeBinding
import com.orem.cam.love.ui.rating.ReviewActivity
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse

class ChatActivity : BaseActivity(), ChatAdapter.Callback {

    private lateinit var binding: ActivityChatBinding
    private lateinit var adapter: ChatAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat)
        inIt()
        inItListener()
    }

    private fun inItListener() {
        binding.ivBack.setOnClickListener { onBackPressed() }
        binding.ivMenu.setOnClickListener { openPopUpMenu() }

    }

    private fun inIt() {
        adapter = ChatAdapter(this)
        binding.rvChat.adapter = adapter
    }


    private fun navigateReview() {
        val intent = Intent(this, ReviewActivity::class.java)
        startActivity(intent)
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

    override fun onClickAdapter(position: Int) {

    }

    private fun showCustomDialog(
        title: String, message: String, actionPositive: String, actionNegative: String
    ) {
        val dialogBinding: DialogOverAgeBinding? = DataBindingUtil.inflate(
            LayoutInflater.from(this), R.layout.dialog_over_age, null, false
        )

        val customDialog = AlertDialog.Builder(this, R.style.CustomAlertDialog).create()

        customDialog.apply {
            setView(dialogBinding?.root)
            setCancelable(false)
        }.show()
        dialogBinding?.tvLabelMessage?.text = message
        dialogBinding?.tvLabelTitle?.text = title
        dialogBinding?.btnYes?.text = actionPositive
        dialogBinding?.btnNo?.text = actionNegative

        dialogBinding?.btnYes?.setOnClickListener {
            customDialog.dismiss()
        }

        dialogBinding?.btnNo?.setOnClickListener {
            customDialog.dismiss()
        }
    }

    private fun openPopUpMenu() {
        val popupMenu = PopupMenu(this, binding.ivMenu)
        popupMenu.menuInflater.inflate(R.menu.menu_options_message, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.delete_chat -> {
                    showCustomDialog(
                        title = getString(R.string.dialog_delete_title),
                        message = getString(R.string.dialog_delete_message),
                        actionPositive = getString(R.string.common_confirmation),
                        actionNegative = getString(R.string.common_deny, getString(R.string.delete))
                    )
                }
                R.id.report -> {
                    showCustomDialog(
                        title = getString(R.string.dialog_report_title),
                        message = getString(R.string.dialog_report_message),
                        actionPositive = getString(R.string.common_confirmation),
                        actionNegative = getString(R.string.common_deny, getString(R.string.report))
                    )
                }
                R.id.clear_chat -> {
                    showCustomDialog(
                        title = getString(R.string.dialog_clear_chat_title),
                        message = getString(R.string.dialog_clear_chat_message),
                        actionPositive = getString(R.string.common_confirmation),
                        actionNegative = getString(R.string.common_deny, getString(R.string.clear))
                    )
                }
            }
            true
        }
        popupMenu.show()
    }
}