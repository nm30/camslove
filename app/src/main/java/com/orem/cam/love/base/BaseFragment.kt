package com.orem.cam.love.base

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.orem.cam.love.utils.ProgressDialog
import retrofit.RetrofitCaller
import retrofit.base.APICallback
import retrofit.base.BaseData
import retrofit.communications.APIHelper
import retrofit.utilz.Log
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Manish Bhargav
 */

abstract class BaseFragment : Fragment(), APICallback<BaseData> {

    protected lateinit var progressDialog: ProgressDialog
    protected lateinit var apiHelper: APIHelper
    protected lateinit var base: BaseActivity
    protected var TAG = javaClass.simpleName


    @LayoutRes
    abstract fun getFragmentLayoutResId(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        apiHelper = RetrofitCaller.build(this)
        base = activity as BaseActivity
        progressDialog = ProgressDialog()
        return inflater.inflate(getFragmentLayoutResId(), container, false)
    }

    fun localErrorMessage(editText: EditText, resourceId: Int, labelName: TextView) {
        editText.error = resources.getString(resourceId, labelName.text.toString())
    }

    fun getStringFromEditText(editText: EditText): String {
        return editText.text.toString()
    }

    fun getDateFormatForServer(year: Int, month: Int, dayOfMonth: Int): String {
        val cal: Calendar = Calendar.getInstance()
        cal.set(year, month, dayOfMonth)
        val tz = TimeZone.getTimeZone("UTC")
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.msZ", Locale.US)
        sdf.timeZone = tz
        val formattedDate: String = sdf.format(cal.time)
        Log.e("Base", formattedDate)
        return formattedDate
    }

    fun getDateFormatForServer(date: Date): String {
        val tz = TimeZone.getTimeZone("UTC")
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.msZ", Locale.US)
        sdf.timeZone = tz
        val formattedDate: String = sdf.format(date)
        Log.e("Base", formattedDate)
        return formattedDate
    }

    fun datePickerDialog(yearBack: Int, listener: DatePickerDialog.OnDateSetListener) {
        val c: Calendar = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR) - yearBack
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(base, listener, mYear, mMonth, mDay)
        datePickerDialog.show()
    }

    @SuppressLint("SimpleDateFormat")
    fun currentDate(): String {
        val formatter = SimpleDateFormat("dd/MM/yyyy")
        val date = Date()
        Log.e("Base", formatter.format(date))
        return formatter.format(date)
    }

    fun isServiceRunning(serviceClass: Class<*>): Boolean {
        val manager: ActivityManager =
            base.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

}