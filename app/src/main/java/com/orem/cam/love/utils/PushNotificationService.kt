package com.orem.cam.love.utils

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import retrofit.contants.PrefsConstants
import retrofit.utilz.BaseUtil

class PushNotificationService : FirebaseMessagingService() {

    private val TAG = javaClass.simpleName

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        PrefsManager.get().save(PrefsConstants.FIREBASE_TOKEN, token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d(
            TAG,
            "onMessageReceived() called with: remoteMessage = ${BaseUtil.jsonFromModel(remoteMessage)}"
        )
    }
}