package com.orem.cam.love.base

import android.content.Context
import android.content.DialogInterface.OnClickListener
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.orem.cam.love.R
import com.orem.cam.love.utils.ProgressDialog
import retrofit.RetrofitCaller
import retrofit.base.APICallback
import retrofit.base.BaseData
import retrofit.communications.APIHelper
import retrofit.contants.ServerKey.Companion.VALUE_TOKEN_EXPIRED
import retrofit.utilz.AlertDialogs
import retrofit.utilz.Log

/**
 * Created by Manish Bhargav
 */

abstract class BaseActivity : AppCompatActivity(), APICallback<BaseData> {

    protected lateinit var progressDialog: ProgressDialog
    protected lateinit var activity: BaseActivity
    protected lateinit var apiHelper: APIHelper
    protected var TAG = javaClass.simpleName


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        apiHelper = RetrofitCaller.build(this)
        activity = this
        progressDialog = ProgressDialog()
    }

    fun showSettingsDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(resources.getString(R.string.app_name))
        builder.setCancelable(false)
        builder.setMessage(message)
        builder.setPositiveButton("Settings") { dialog, which ->
            startActivity(
                Intent(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", packageName, null)
                )
            )
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialog, which ->
            dialog.dismiss()
        }
        if (!isFinishing)
            builder.show()
    }

    fun isNetworkAvailableWithMessage(): Boolean {
        val isActive = isNetworkAvailable()
        if (!isActive) {
            shortToast(getString(R.string.error_check_internet_connection))
        }
        return isActive
    }

    fun shortToast(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun longToast(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    fun shortSnackBar(message: String, view: View) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }

    fun longSnackBar(message: String, view: View) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
    }

    fun checkTokenExpired(tag: String, message: String, errorCode: Int) {
        appExit(message, errorCode)
        Log.e("$tag --- failure", "$message --- $errorCode")
    }


    private fun appExit(message: String, errorCode: Int) {
        val dialogClickListener =
            OnClickListener { dialog, which ->
                if (errorCode == VALUE_TOKEN_EXPIRED) {
//                    userLogout()
                }
            }
        AlertDialogs(activity)
            .showOk(activity, message, dialogClickListener)
    }

    private fun isNetworkAvailable(): Boolean {
        var result = false
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }
                }
            }
        }
        return result
    }

    fun checkPermissions(permissions: Array<String>): Boolean {
        for (permission in permissions) {
            if (checkPermission(permission)) {
                return true
            }
        }
        return false
    }

    fun checkPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_DENIED
    }

}