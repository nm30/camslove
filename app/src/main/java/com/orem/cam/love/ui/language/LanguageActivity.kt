package com.orem.cam.love.ui.language

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityLanguageBinding
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.model.Language

class LanguageActivity : BaseActivity(), LanguageAdapter.Callback {

    private lateinit var binding: ActivityLanguageBinding
    private lateinit var adapter: LanguageAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_language)
        inIt()
        inItListener()
    }

    private fun inIt() {
        adapter = LanguageAdapter(this)
        binding.rvLanguages.adapter = adapter
        adapter.setItems(getList())
    }

    private fun getList(): ArrayList<Language> {
        val list = arrayListOf<Language>()
        list.add(
            Language(
                id = "1", title = getString(R.string.english),
                url = R.drawable.ic_flag_en, isSelected = true
            )
        )
        list.add(
            Language(
                id = "2", title = getString(R.string.portuguese),
                url = R.drawable.ic_flag_brazil, isSelected = false
            )
        )
        return list
    }

    private fun inItListener() {
        binding.ivBack.setOnClickListener { onBackPressed() }
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

    override fun onClickAdapter(position: Int) {

    }
}