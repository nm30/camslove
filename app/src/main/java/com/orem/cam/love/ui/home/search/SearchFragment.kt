package com.orem.cam.love.ui.home.search

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseFragment
import com.orem.cam.love.databinding.FragmentSearchBinding
import com.orem.cam.love.ui.call.VideoCallActivity
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse

class SearchFragment : BaseFragment() {

    private lateinit var binding: FragmentSearchBinding

    override fun getFragmentLayoutResId() = R.layout.fragment_search

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.bind(view)!!
        inIt()
        inItListener()
    }

    private fun inIt() {

    }

    private fun inItListener() {
        binding.btnLoginRegister.setOnClickListener { }
        binding.btnStartSearching.setOnClickListener { navigateVideoCall() }
    }

    private fun navigateVideoCall() {
        val intent = Intent(base, VideoCallActivity::class.java)
        startActivity(intent)
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

}