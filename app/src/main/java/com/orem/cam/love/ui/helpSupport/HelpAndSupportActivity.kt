package com.orem.cam.love.ui.helpSupport

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityHelpAndSupportBinding
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.model.NotificationAndSupport

class HelpAndSupportActivity : BaseActivity(), HelpAndSupportAdapter.Callback {

    private lateinit var binding: ActivityHelpAndSupportBinding
    private lateinit var adapter: HelpAndSupportAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_help_and_support)
        inIt()
        inItListener()
    }

    private fun inIt() {
        adapter = HelpAndSupportAdapter(this)
        binding.rvHelpAndSupport.adapter = adapter
        adapter.setItems(getList())
    }

    private fun getList(): ArrayList<NotificationAndSupport> {
        val list = arrayListOf<NotificationAndSupport>()
        list.add(
            NotificationAndSupport(
                id = "1", title = getString(R.string.help_support_video_chat_title),
                description = getString(R.string.help_support_video_chat_message)
            )
        )
        list.add(
            NotificationAndSupport(
                id = "2", title = getString(R.string.help_support_payment_title),
                description = getString(R.string.help_support_payment_message)
            )
        )
        list.add(
            NotificationAndSupport(
                id = "3", title = getString(R.string.help_support_general_title),
                description = getString(R.string.help_support_general_message)
            )
        )
        return list
    }

    private fun inItListener() {
        binding.ivBack.setOnClickListener { onBackPressed() }
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

    override fun onClickAdapter(position: Int) {

    }
}