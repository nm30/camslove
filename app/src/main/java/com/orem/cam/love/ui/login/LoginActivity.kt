package com.orem.cam.love.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityLoginBinding
import com.orem.cam.love.ui.forgot.ForgotActivity
import com.orem.cam.love.ui.home.HomeActivity
import com.orem.cam.love.ui.signUp.SignUpActivity
import com.orem.cam.love.utils.PrefsManager
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.contants.Config
import retrofit.contants.PrefsConstants
import retrofit.datamodel.login.UserLoginResponse
import retrofit.datamodel.login.UserRegister
import retrofit.utilz.BaseUtil
import retrofit.utilz.Log

class LoginActivity : BaseActivity() {

    private lateinit var binding: ActivityLoginBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        inIt()
        inItListener()
    }

    private fun inIt() {
//        binding.edtEmail.setText("arjun@yopmail.com")
//        binding.edtPassword.setText("12345678")
    }

    private fun inItListener() {
        binding.btnLogin.setOnClickListener { navigateHome() }
        binding.ivBack.setOnClickListener { onBackPressed() }
        binding.tvRegister.setOnClickListener { navigateToSignUp() }
        binding.tvForgotPassword.setOnClickListener { navigateToForgot() }
    }

    private fun navigateToSignUp() {
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToForgot() {
        val intent = Intent(this, ForgotActivity::class.java)
        startActivity(intent)
    }

    private fun navigateHome() {

        if (!validation()) return
        callApi(
            UserRegister(
                email = binding.edtEmail.text.toString(),
                password = binding.edtPassword.text.toString(),
                deviceToken = PrefsManager.get()
                    .getString(PrefsConstants.FIREBASE_TOKEN, getString(R.string.default_value)),
                deviceType = Config.deviceType
            )
        )
    }

    private fun callApi(data: UserRegister) {
        if (isNetworkAvailableWithMessage()) {
            progressDialog.showLoadingDialog(this, null)
            apiHelper.userLogin(data)
        }
    }

    private fun validation(): Boolean {
        var isValid = true
        if (binding.edtEmail.text.toString().isEmpty() ||
            !Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text.toString()).matches()
        ) {
            binding.edtEmail.error = getString(R.string.error_empty_email)
            isValid = false
        }
        if (binding.edtPassword.text.toString().isEmpty()) {
            binding.edtPassword.error = getString(R.string.error_empty_password)
            isValid = false
        }

        return isValid
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        when (response.getResponseType()) {
            ResponseType.USER_LOGIN -> {
                progressDialog.dismissLoadingDialog()
                val data = response.getData() as UserLoginResponse
                Log.d(TAG, " response = ${BaseUtil.jsonFromModel(data)}")
                shortSnackBar(response.getMessage() ?: "", binding.root)
                shortToast(response.getMessage() ?: "")
                PrefsManager.get().save(PrefsConstants.ACCESS_TOKEN, data.token ?: "")
                PrefsManager.get().save(PrefsConstants.IS_LOGIN, true)
                PrefsManager.get().save(PrefsConstants.USER_DETAILS, BaseUtil.jsonFromModel(data))
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }

        }
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        progressDialog.dismissLoadingDialog()
        Log.d(TAG, "$message, $errorCode, $responseType")
        shortSnackBar(message, binding.root)
    }
}