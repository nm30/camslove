package com.orem.cam.love.base

import android.view.View
import androidx.annotation.Keep
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Manish Bhargav
 */

@Keep
abstract class BaseAdapter<ViewHolder : RecyclerView.ViewHolder> :
    RecyclerView.Adapter<ViewHolder>() {
    protected var TAG = javaClass.simpleName

    private var lastPosition = -1
    protected fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
//            val animation = AnimationUtils.loadAnimation(viewToAnimate.context, R.anim.slide_in_bottom)
//            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    open fun updatePosition(position: Int) {

    }

    open fun clearUpdate(position: Int) {

    }


}
