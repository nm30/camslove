package com.orem.cam.love.ui.home.message

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseAdapter
import com.orem.cam.love.databinding.ItemFilterOptionsBinding
import retrofit.model.FilterOptions
import retrofit.utilz.Log

/**
 * Created by Manish Bhargav
 */

class FilterAdapter(private val callback: Callback) :
    BaseAdapter<FilterAdapter.ViewHolder>() {

    private val items = ArrayList<FilterOptions>()
    private var selectedIndex = 0
    private lateinit var binding: ItemFilterOptionsBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_filter_options, parent, false
        )
        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(binding.root, callback)

    }

    override fun getItemCount(): Int {
        Log.e(TAG, "-------- ${items.size}")
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item, position)
    }

    inner class ViewHolder(itemView: View, private val callback: Callback) :
        RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                callback.onClickFilterAdapter(adapterPosition)
                notifyDataSetChanged()
            }
        }

        fun bind(data: FilterOptions, index: Int) {
            binding.rbItemFilter.text = data.title
            binding.rbItemFilter.isChecked = data.isActive == true
        }
    }

    fun setItems(list: List<FilterOptions>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun addItems(newItems: List<FilterOptions>) {
        val oldListSize = this.items.size
        this.items.addAll(newItems)
        notifyItemRangeInserted(oldListSize, newItems.size)
    }

    fun getItems(position: Int): FilterOptions {
        return items[position]
    }

    interface Callback {
        fun onClickFilterAdapter(position: Int)
    }
}