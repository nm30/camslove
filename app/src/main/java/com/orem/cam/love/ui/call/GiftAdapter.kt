package com.orem.cam.love.ui.call

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseAdapter
import com.orem.cam.love.databinding.ItemSendGiftBinding
import retrofit.model.GiftOptions
import retrofit.utilz.Log

/**
 * Created by Manish Bhargav
 */

class GiftAdapter(private val callback: Callback) :
    BaseAdapter<GiftAdapter.ViewHolder>() {

    private val items = ArrayList<GiftOptions>()
    private var selectedIndex = 0
    private lateinit var binding: ItemSendGiftBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_send_gift, parent, false
        )
        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(binding.root, callback)

    }

    override fun getItemCount(): Int {
        Log.e(TAG, "-------- ${items.size}")
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item, position)
    }

    inner class ViewHolder(itemView: View, private val callback: Callback) :
        RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                callback.onClickGiftAdapter(adapterPosition)
                notifyDataSetChanged()
            }
        }

        fun bind(data: GiftOptions, index: Int) {
            binding.tvItemName.text = data.title
            binding.tvItemMessage.text = data.description
            binding.ivItemGift.setImageDrawable(data.url?.let {
                ContextCompat.getDrawable(
                    itemView.context, it
                )
            })
        }
    }

    fun setItems(list: List<GiftOptions>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun addItems(newItems: List<GiftOptions>) {
        val oldListSize = this.items.size
        this.items.addAll(newItems)
        notifyItemRangeInserted(oldListSize, newItems.size)
    }

    fun getItems(position: Int): GiftOptions {
        return items[position]
    }

    interface Callback {
        fun onClickGiftAdapter(position: Int)
    }
}