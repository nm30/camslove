package com.orem.cam.love.ui.profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityCreateProfileBinding
import com.orem.cam.love.databinding.DialogOverAgeBinding
import com.orem.cam.love.ui.home.HomeActivity
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.contants.PrefsConstants.Companion.CREATE_PROFILE
import retrofit.contants.PrefsConstants.Companion.GENDER_FEMALE
import retrofit.contants.PrefsConstants.Companion.GENDER_MALE
import java.util.*

class ProfileActivity : BaseActivity() {

    private lateinit var binding: ActivityCreateProfileBinding
    private var flag = 0
    private var gender = 0

    companion object {
        private const val EXTRA_FLAG = "EXTRA_FLAG"
        fun getProfileIntent(context: Context, flag: Int): Intent {
            return Intent(context, ProfileActivity::class.java)
                .putExtra(EXTRA_FLAG, flag)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_profile)
        inIt()
        inItListener()
    }

    private fun inIt() {
        flag = intent.getIntExtra(EXTRA_FLAG, 0)

        if (flag == CREATE_PROFILE) {
            binding.tvTitle.visibility = View.GONE
            binding.tvLabelWelcome.visibility = View.VISIBLE
            binding.tvLabelMessage.visibility = View.VISIBLE
            binding.btnUpdate.setText(R.string.done)
        } else {
            binding.tvTitle.visibility = View.VISIBLE
            binding.tvLabelWelcome.visibility = View.GONE
            binding.tvLabelMessage.visibility = View.GONE
            binding.btnUpdate.setText(R.string.update)
        }
        selectGender(gender)
        dob()
    }

    private fun selectGender(gender: Int) {
        if (gender == GENDER_MALE) {
            binding.clMale.background =
                ContextCompat.getDrawable(this, R.drawable.bg_rectangle_corner_gender_selected)
            binding.clFemale.background =
                ContextCompat.getDrawable(this, R.drawable.bg_rectangle_corner_gender_un_selected)
        } else {
            binding.clMale.background =
                ContextCompat.getDrawable(this, R.drawable.bg_rectangle_corner_gender_un_selected)
            binding.clFemale.background =
                ContextCompat.getDrawable(this, R.drawable.bg_rectangle_corner_gender_selected)
        }
    }

    private fun inItListener() {
        binding.ivBack.setOnClickListener { onBackPressed() }
        binding.clFemale.setOnClickListener {
            gender = GENDER_FEMALE
            selectGender(gender)
        }
        binding.clMale.setOnClickListener {
            gender = GENDER_MALE
            selectGender(gender)
        }
        binding.btnUpdate.setOnClickListener {
            if (flag == CREATE_PROFILE) {
                showCustomDialog(
                    title = getString(R.string.are_you_over_18),
                    message = getString(R.string.alert_age_message),
                    actionPositive = getString(R.string.yes_i_am),
                )
            } else {
                onBackPressed()
            }
        }
    }

    private fun navigateHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    private fun showCustomDialog(title: String, message: String, actionPositive: String) {
        val dialogBinding: DialogOverAgeBinding? = DataBindingUtil.inflate(
            LayoutInflater.from(this), R.layout.dialog_over_age, null, false
        )

        val customDialog = AlertDialog.Builder(this, R.style.CustomAlertDialog).create()

        customDialog.apply {
            setView(dialogBinding?.root)
            setCancelable(false)
        }.show()
        dialogBinding?.tvLabelMessage?.text = message
        dialogBinding?.tvLabelTitle?.text = title
        dialogBinding?.btnYes?.text = actionPositive
        dialogBinding?.btnNo?.visibility = View.GONE

        dialogBinding?.btnYes?.setOnClickListener {
            customDialog.dismiss()
            navigateHome()
        }

    }

    private fun validation(): Boolean {
        var isValid = true
        if (binding.edtNickname.text.toString().isEmpty()) {
            binding.edtNickname.error = getString(R.string.error_empty_nickname)
            isValid = false
        }

        return isValid
    }

    private fun dob() {
        val today = Calendar.getInstance()
        binding.dpDob.init(
            today.get(Calendar.YEAR), today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)
        ) { view, year, month, day ->
            val months = month + 1
            val msg = "You selected: $day/$months/$year"
            shortToast(msg)
        }
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }
}