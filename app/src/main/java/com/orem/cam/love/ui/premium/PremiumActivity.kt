package com.orem.cam.love.ui.premium

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityLanguageBinding
import com.orem.cam.love.databinding.ActivityPremiumBinding
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.model.Language

class PremiumActivity : BaseActivity(), PremiumAdapter.Callback {

    private lateinit var binding: ActivityPremiumBinding
    private lateinit var adapter: PremiumAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_premium)
        inIt()
        inItListener()
    }

    private fun inIt() {
        adapter = PremiumAdapter(this)
        binding.rvPremiumPlans.adapter = adapter
    }


    private fun inItListener() {
        binding.ivBack.setOnClickListener { onBackPressed() }
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

    override fun onClickAdapter(position: Int) {

    }
}