package com.orem.cam.love.ui.home.settings

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseFragment
import com.orem.cam.love.databinding.FragmentSettingsBinding
import com.orem.cam.love.ui.about.AboutUsActivity
import com.orem.cam.love.ui.helpSupport.HelpAndSupportActivity
import com.orem.cam.love.ui.language.LanguageActivity
import com.orem.cam.love.ui.notifications.NotificationSettingsActivity
import com.orem.cam.love.ui.premium.PremiumActivity
import com.orem.cam.love.ui.profile.ProfileActivity
import com.orem.cam.love.ui.signUp.SignUpTypeActivity
import com.orem.cam.love.utils.PrefsManager
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.contants.PrefsConstants
import retrofit.contants.PrefsConstants.Companion.ACCOUNT_SETTINGS

class SettingsFragment : BaseFragment() {

    private lateinit var binding: FragmentSettingsBinding

    override fun getFragmentLayoutResId() = R.layout.fragment_settings

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.bind(view)!!
        inIt()
        inItListener()
    }

    private fun inIt() {

    }

    private fun inItListener() {
        binding.btnAboutUs.setOnClickListener { navigateAboutUs() }
        binding.btnHelpSupport.setOnClickListener { navigateHelpAndSupport() }
        binding.btnNotificationSettings.setOnClickListener { navigateNotificationSettings() }
        binding.btnLanguage.setOnClickListener { navigateLanguage() }
        binding.btnAccountSettings.setOnClickListener { navigateAccountSettings() }
        binding.btnGoPremium.setOnClickListener { navigatePremium() }
        binding.llActivatePlan.setOnClickListener { navigatePremium() }
        binding.btnPaymentHistory.setOnClickListener { base.shortToast(getString(R.string.development_process)) }
        binding.btnLogout.setOnClickListener { userLogout() }
    }

    private fun navigateAboutUs() {
        val intent = AboutUsActivity.getAboutUsIntent(base, PrefsConstants.ABOUT_US)
        startActivity(intent)
    }

    private fun navigateHelpAndSupport() {
        val intent = Intent(base, HelpAndSupportActivity::class.java)
        startActivity(intent)
    }

    private fun navigateNotificationSettings() {
        val intent = Intent(base, NotificationSettingsActivity::class.java)
        startActivity(intent)
    }

    private fun navigateLanguage() {
        val intent = Intent(base, LanguageActivity::class.java)
        startActivity(intent)
    }

    private fun navigateAccountSettings() {
        val intent = ProfileActivity.getProfileIntent(base, ACCOUNT_SETTINGS)
        startActivity(intent)
    }

    private fun navigatePremium() {
        val intent = Intent(base, PremiumActivity::class.java)
        startActivity(intent)
    }

    private fun userLogout() {
        PrefsManager.get().removeAll()
        val intent = Intent(base, SignUpTypeActivity::class.java)
        startActivity(intent)
        base.finishAffinity()
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

}