package com.orem.cam.love.ui.home.message

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseFragment
import com.orem.cam.love.databinding.FragmentMessagesBinding
import com.orem.cam.love.ui.chat.ChatActivity
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.model.FilterOptions

class MessagesFragment : BaseFragment(), MessagesAdapter.Callback, FilterAdapter.Callback {

    private lateinit var binding: FragmentMessagesBinding
    private lateinit var adapter: MessagesAdapter
    private lateinit var adapterFilter: FilterAdapter

    override fun getFragmentLayoutResId() = R.layout.fragment_messages

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.bind(view)!!
        inIt()
        inItListener()
    }

    private fun inIt() {
        adapter = MessagesAdapter(this)
        binding.rvMessages.adapter = adapter
        setFilterAdapter()
    }

    private fun inItListener() {
        binding.ivFilter.setOnClickListener { binding.drawerLayout.openDrawer(Gravity.RIGHT) }
        binding.ivClose.setOnClickListener { binding.drawerLayout.closeDrawer(Gravity.RIGHT) }
        binding.btnApply.setOnClickListener { binding.drawerLayout.closeDrawer(Gravity.RIGHT) }
        binding.btnReset.setOnClickListener { binding.drawerLayout.closeDrawer(Gravity.RIGHT) }
    }

    private fun setFilterAdapter() {
        adapterFilter = FilterAdapter(this)
        binding.rvFilterOptions.adapter = adapterFilter
        adapterFilter.setItems(getList())
    }

    private fun getList(): ArrayList<FilterOptions> {
        val list = arrayListOf<FilterOptions>()
        list.add(
            FilterOptions(
                id = "1", title = getString(R.string.filter_option_1), isActive = true
            )
        )
        list.add(
            FilterOptions(
                id = "2", title = getString(R.string.filter_option_2), isActive = false
            )
        )
        list.add(
            FilterOptions(
                id = "3", title = getString(R.string.filter_option_3), isActive = false
            )
        )
        list.add(
            FilterOptions(
                id = "4", title = getString(R.string.filter_option_4), isActive = false
            )
        )
        list.add(
            FilterOptions(
                id = "5", title = getString(R.string.filter_option_5), isActive = false
            )
        )
        return list
    }

    private fun navigateChat() {
        val intent = Intent(base, ChatActivity::class.java)
        startActivity(intent)
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

    override fun onClickAdapter(position: Int) {
        navigateChat()
    }

    override fun onClickFilterAdapter(position: Int) {

    }
}