package com.orem.cam.love.utils

import android.app.Activity
import android.app.Dialog
import android.util.Log
import android.widget.TextView
import com.orem.cam.love.R

/**
 * Created by Manish Bhargav
 */

class ProgressDialog {

    private var dialog: Dialog? = null
    private val TAG = javaClass.simpleName

    fun showLoadingDialog(
        context: Activity,
        message: String?
    ) {
        var message = message
        Log.d(
            TAG, "showLoadingDialog() called with: context =" + context.toString() +
                    "," + "message = " + message
        )
        if (message == null || message.isEmpty()) message = context.getString(R.string.loading)
        if (isDialogShowing()) {
            dismissLoadingDialog()
        }
        if (context.isFinishing) {
            return
        }
        dialog = Dialog(context, R.style.Theme_AppCompat_Dialog_Alert)
        dialog?.setContentView(R.layout.dialog_loading)
        val title: TextView =
            dialog!!.findViewById(R.id.tvTitle)
        title.text = message
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }

    private fun isDialogShowing(): Boolean {
        return try {
            if (dialog == null) {
                false
            } else {
                dialog!!.isShowing
            }
        } catch (e: Exception) {
            false
        }
    }

    fun dismissLoadingDialog() {
        Log.d(TAG, "dismissLoadingDialog() called")
        try {
            if (dialog != null) {
                dialog!!.dismiss()
                dialog = null
            }
        } catch (e: Exception) {
            Log.e("e", "=\$e")
        }
    }


}