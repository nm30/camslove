package com.orem.cam.love.ui.call

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityVideoCallBinding
import com.orem.cam.love.databinding.DialogSendAmountBinding
import com.orem.cam.love.databinding.DialogSendGiftBinding
import com.orem.cam.love.ui.rating.ReviewActivity
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.model.GiftOptions

class VideoCallActivity : BaseActivity(), LiveChatAdapter.Callback, GiftAdapter.Callback {

    private lateinit var binding: ActivityVideoCallBinding
    private lateinit var adapter: LiveChatAdapter
    private lateinit var adapterGift: GiftAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video_call)
        inIt()
        inItListener()
    }

    private fun inItListener() {
        binding.ivBack.setOnClickListener { onBackPressed() }
        binding.btnStartSearching.setOnClickListener { videoConnected() }
        binding.ivCallDisconnect.setOnClickListener { navigateReview() }
        binding.ivLiveMessage.setOnClickListener { liveChat() }
        binding.ivGift.setOnClickListener { showGiftDialog() }
        binding.ivPayment.setOnClickListener { showPaymentDialog() }
    }

    private fun inIt() {
        adapter = LiveChatAdapter(this)
        binding.rvLiveChat.adapter = adapter
    }

    private fun videoConnected() {
        binding.btnStartSearching.visibility = View.GONE
        binding.ivLiveMessage.visibility = View.VISIBLE
        binding.ivPayment.visibility = View.VISIBLE
        binding.ivGift.visibility = View.VISIBLE
        binding.btnNext.visibility = View.VISIBLE
        binding.flOtherVideo.visibility = View.VISIBLE
        binding.ivAddParticipant.visibility = View.VISIBLE
        binding.ivCallDisconnect.visibility = View.VISIBLE
        binding.flOwnVideo.background = ContextCompat.getDrawable(this, R.drawable.bg_other_video)
    }

    private fun liveChat() {
        binding.ivSpeaker.visibility = View.GONE
        binding.ivMic.visibility = View.GONE
        binding.ivChangeCamera.visibility = View.GONE
        binding.ivLiveMessage.visibility = View.GONE
        binding.clSendMessage.visibility = View.VISIBLE
        binding.rvLiveChat.visibility = View.VISIBLE
    }

    private fun navigateReview() {
        val intent = Intent(this, ReviewActivity::class.java)
        startActivity(intent)
    }

    private fun getList(): ArrayList<GiftOptions> {
        val list = arrayListOf<GiftOptions>()
        list.add(
            GiftOptions(
                id = "1", title = getString(R.string.gift_bear_title),
                url = R.drawable.ic_bear, description = getString(R.string.gift_bear_message)
            )
        )
        list.add(
            GiftOptions(
                id = "2", title = getString(R.string.gift_flower_title),
                url = R.drawable.ic_flower, description = getString(R.string.gift_flower_message)
            )
        )
        list.add(
            GiftOptions(
                id = "3", title = getString(R.string.gift_candy_title),
                url = R.drawable.ic_candy, description = getString(R.string.gift_candy_message)
            )
        )
        list.add(
            GiftOptions(
                id = "4", title = getString(R.string.gift_diamond_title),
                url = R.drawable.ic_diamond, description = getString(R.string.gift_diamond_message)
            )
        )
        return list
    }

    private fun showGiftDialog() {
        val dialogBinding: DialogSendGiftBinding? = DataBindingUtil.inflate(
            LayoutInflater.from(this), R.layout.dialog_send_gift, null, false
        )
        val customDialog = AlertDialog.Builder(this, R.style.SendAmountDialog).create()

        customDialog.apply {
            setView(dialogBinding?.root)
            setCancelable(false)
        }.show()

        adapterGift = GiftAdapter(this)
        dialogBinding?.rvGifts?.adapter = adapterGift
        adapterGift.setItems(getList())

        dialogBinding?.ivClose?.setOnClickListener {
            customDialog.dismiss()
        }
    }

    private fun showPaymentDialog() {
        val dialogBinding: DialogSendAmountBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this), R.layout.dialog_send_amount, null, false
        )

        val customDialog = AlertDialog.Builder(this, R.style.SendAmountDialog).create()

        customDialog.apply {
            setView(dialogBinding?.root)
            setCancelable(false)
        }.show()

        dialogBinding.btnYes.setOnClickListener {
            customDialog.dismiss()
        }

        dialogBinding.ivClose.setOnClickListener {
            customDialog.dismiss()
        }
    }


    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

    override fun onClickAdapter(position: Int) {

    }

    override fun onClickGiftAdapter(position: Int) {

    }
}