package com.orem.cam.love.ui.language

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivitySelectLanguageBinding
import com.orem.cam.love.ui.signUp.SignUpTypeActivity
import com.orem.cam.love.utils.PrefsManager
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.contants.PrefsConstants

class SelectLanguageActivity : BaseActivity() {

    private lateinit var binding: ActivitySelectLanguageBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_language)

        inItListener()
    }

    private fun inItListener() {
        binding.btnEng.setOnClickListener { navigate(getString(R.string.english)) }
        binding.btnPor.setOnClickListener { navigate(getString(R.string.portuguese)) }
    }

    private fun navigate(selectedLanguage: String) {
        PrefsManager.get().save(PrefsConstants.SELECTED_LANGUAGE, selectedLanguage)
        PrefsManager.get().save(PrefsConstants.IS_SELECTED_LANGUAGE, true)
        val intent = Intent(this, SignUpTypeActivity::class.java)
        startActivity(intent)
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }
}