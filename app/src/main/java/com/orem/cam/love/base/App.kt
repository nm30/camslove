package com.orem.cam.love.base

import android.app.Application
import com.google.firebase.FirebaseApp
import com.orem.cam.love.utils.PrefsManager


class App : Application() {

    companion object {
        private lateinit var instance: App

        @Synchronized
        fun getInstance(): App = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        FirebaseApp.initializeApp(this)
        PrefsManager.initialize(this)
    }


}