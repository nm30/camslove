package com.orem.cam.love.ui.forgot

import android.os.Bundle
import android.util.Patterns
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityForgotBinding
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.utilz.Log

class ForgotActivity : BaseActivity() {

    private lateinit var binding: ActivityForgotBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot)

        inItListener()
    }

    private fun inItListener() {
        binding.btnSend.setOnClickListener { navigate() }
        binding.ivBack.setOnClickListener { onBackPressed() }

    }

    private fun navigate() {
        if (!validation()) return
        callApi(binding.edtEmail.text.toString())
    }

    private fun validation(): Boolean {
        var isValid = true
        if (binding.edtEmail.text.toString().isEmpty() ||
            !Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.text.toString()).matches()
        ) {
            binding.edtEmail.error = getString(R.string.error_empty_email)
            isValid = false
        }

        return isValid
    }

    private fun callApi(data: String) {
        if (isNetworkAvailableWithMessage()) {
            progressDialog.showLoadingDialog(this, null)
            apiHelper.forgotPassword(data)
        }
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        when (response.getResponseType()) {
            ResponseType.FORGOT_PASSWORD -> {
                progressDialog.dismissLoadingDialog()
                shortSnackBar(response.getMessage() ?: "", binding.root)
            }

        }
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        progressDialog.dismissLoadingDialog()
        Log.d(TAG, "$message, $errorCode, $responseType")
        shortSnackBar(message, binding.root)
    }
}