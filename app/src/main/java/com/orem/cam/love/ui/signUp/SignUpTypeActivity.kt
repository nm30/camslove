package com.orem.cam.love.ui.signUp

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivitySignUpTypeBinding
import com.orem.cam.love.databinding.DialogGenderSelectionBinding
import com.orem.cam.love.ui.about.AboutUsActivity
import com.orem.cam.love.ui.login.LoginActivity
import com.orem.cam.love.ui.profile.ProfileActivity
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.contants.PrefsConstants
import retrofit.utilz.BaseUtil
import retrofit.utilz.Log


class SignUpTypeActivity : BaseActivity() {

    private lateinit var binding: ActivitySignUpTypeBinding
    lateinit var mGoogleSignInClient: GoogleSignInClient

    private val googleSignInLauncher =
        registerForActivityResult(StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val task: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(result.data)
                handleSignInResult(task)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up_type)
        inIt()
        inItListener()
    }

    private fun inIt() {
        spanString(
            binding.tvTerms, getString(R.string.label_terms), getString(R.string.terms),
            getString(R.string.and), getString(R.string.privacy)
        )
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestProfile()
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun inItListener() {
        binding.btnCreateAccount.setOnClickListener { navigateToSignUp() }
        binding.btnLogin.setOnClickListener { navigateToLogin() }
        binding.btnFb.setOnClickListener { navigateCreateAccount() }
        binding.btnGoogle.setOnClickListener { navigateCreateAccount() }
        binding.clMsn.setOnClickListener { navigateCreateAccount() }
        binding.tvSkip.setOnClickListener { showSkipGenderDialog() }
//        binding.btnVk.setOnClickListener { navigateCreateAccount() }
//        binding.btnYandex.setOnClickListener { navigateCreateAccount() }
        binding.ivBack.setOnClickListener { onBackPressed() }
    }


    /**
     * This method is used for login with google account
     * */
    private fun signIn() {
        googleSignInLauncher.launch(mGoogleSignInClient.signInIntent)
    }

    private fun handleSignInResult(task: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount = task.getResult(ApiException::class.java)
            // Signed in successfully, show authenticated UI.
            Log.d(TAG, "handleSignInResult() called with: task = ${account.email}")
            Log.d(
                TAG,
                "handleSignInResult() called with: task = ${BaseUtil.jsonFromModel(account)}"
            )
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
        }
    }

    private fun navigateToLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToSignUp() {
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
    }

    private fun navigateToAbout(flag: Int) {
        val intent = AboutUsActivity.getAboutUsIntent(this, flag)
        startActivity(intent)
    }

    private fun navigateCreateAccount() {
        val intent = ProfileActivity.getProfileIntent(this, PrefsConstants.CREATE_PROFILE)
        startActivity(intent)
    }

    private fun spanString(
        textView: TextView, label: String, terms: String, and: String, privacy: String
    ) {
        val spanText = SpannableStringBuilder()
        spanText.append(label)
        spanText.append(" ")
        spanText.append(terms)
        spanText.setSpan(object : ClickableSpan() {
            override fun updateDrawState(textPaint: TextPaint) {
                textPaint.color =
                    Color.parseColor("#000000")//textPaint.linkColor // you can use custom color
                textPaint.isUnderlineText = false // this remove the underline
            }

            override fun onClick(widget: View) {
                navigateToAbout(PrefsConstants.TERMS_AND_CONDITION)
            }
        }, spanText.length - terms.length, spanText.length, 0)
        spanText.append(" ")
        spanText.append(and)
        spanText.append(" ")
        spanText.append(privacy)
        spanText.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {

                navigateToAbout(PrefsConstants.PRIVACY_POLICY)
            }

            override fun updateDrawState(textPaint: TextPaint) {
                textPaint.color = Color.parseColor("#000000") // you can use custom color
                textPaint.isUnderlineText = false // this remove the underline
            }
        }, spanText.length - privacy.length, spanText.length, 0)
        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.setText(spanText, TextView.BufferType.SPANNABLE)
    }


    /**
     * This method used selected the user gender & proceed for home without login
     * */
    private fun showSkipGenderDialog() {
        val dialogBinding: DialogGenderSelectionBinding? = DataBindingUtil.inflate(
            LayoutInflater.from(this), R.layout.dialog_gender_selection, null, false
        )

        val customDialog = AlertDialog.Builder(this, R.style.CustomAlertDialog).create()

        customDialog.apply {
            setView(dialogBinding?.root)
            setCancelable(false)
        }.show()

        dialogBinding?.clFemale?.setOnClickListener {
            customDialog.dismiss()
        }

        dialogBinding?.ivClose?.setOnClickListener {
            customDialog.dismiss()
        }

        dialogBinding?.clMale?.setOnClickListener {
            customDialog.dismiss()
        }
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }
}