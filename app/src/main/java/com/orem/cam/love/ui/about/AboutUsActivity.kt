package com.orem.cam.love.ui.about

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import com.orem.cam.love.R
import com.orem.cam.love.base.BaseActivity
import com.orem.cam.love.databinding.ActivityAboutUsBinding
import retrofit.ResponseType
import retrofit.base.BaseData
import retrofit.base.BaseResponse
import retrofit.contants.PrefsConstants.Companion.PRIVACY_POLICY
import retrofit.contants.PrefsConstants.Companion.PRIVACY_POLICY_URL
import retrofit.contants.PrefsConstants.Companion.TERMS_AND_CONDITION
import retrofit.contants.PrefsConstants.Companion.TERMS_AND_CONDITION_URL

class AboutUsActivity : BaseActivity() {

    private lateinit var binding: ActivityAboutUsBinding
    private var flag = 0

    companion object {
        private const val EXTRA_FLAG = "EXTRA_FLAG"
        fun getAboutUsIntent(context: Context, flag: Int): Intent {
            return Intent(context, AboutUsActivity::class.java)
                .putExtra(EXTRA_FLAG, flag)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_about_us)
        inIt()
        inItListener()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun inIt() {
        flag = intent.getIntExtra(EXTRA_FLAG, 0)

        // WebViewClient allows you to handle
        // onPageFinished and override Url loading.
        binding.wvTermsPrivacy.webViewClient = ProgressWebViewClient()
        // this will enable the javascript settings
        binding.wvTermsPrivacy.settings.javaScriptEnabled = true

        // if you want to enable zoom feature
        binding.wvTermsPrivacy.settings.setSupportZoom(true)
        when (flag) {
            PRIVACY_POLICY -> {
                binding.progressBar.visibility = View.VISIBLE
                binding.tvTitle.setText(R.string.privacy)
                // this will load the url of the website
                binding.wvTermsPrivacy.loadUrl(PRIVACY_POLICY_URL)
            }
            TERMS_AND_CONDITION -> {
                binding.progressBar.visibility = View.VISIBLE
                binding.tvTitle.setText(R.string.terms)
                // this will load the url of the website
                binding.wvTermsPrivacy.loadUrl(TERMS_AND_CONDITION_URL)
            }
            else -> {
                binding.progressBar.visibility = View.GONE
                binding.wvTermsPrivacy.visibility = View.GONE
                binding.ivLogo.visibility = View.VISIBLE
                binding.tvAppVersion.visibility = View.VISIBLE
                binding.tvAbout.visibility = View.VISIBLE
            }
        }
    }

    private fun inItListener() {
        binding.ivBack.setOnClickListener { onBackPressed() }
    }

    override fun onSuccessfullyCallModel(response: BaseResponse<BaseData>) {
        TODO("Not yet implemented")
    }

    override fun onFailureCall(message: String, errorCode: Int, responseType: ResponseType) {
        TODO("Not yet implemented")
    }

    inner class ProgressWebViewClient : WebViewClient() {

        // Load the URL
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return false
        }

        // ProgressBar will disappear once page is loaded
        override fun onPageFinished(view: WebView, url: String) {
            binding.progressBar.visibility = View.GONE
            super.onPageFinished(view, url)

        }

    }
}